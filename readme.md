# CEDS Admin Backend
## OVERVIEW
	this app handles the admin operations for the CEDS Apps. 

# INSTALLATION
1. clone git repository
2. navigate to project directory
3. run the 'composer install'
4. migrate the database, run 'php artisan migrate'
5. seed the database for initial value, run 'php artisan db:seed'
6. install memcached & php memcached extension on server
7. install imagick dan php imagick extension on server


# VERSION
## 0.1 : dubbed as the first iteration of the sprint
	operation related to category (CRUD), dummy dashboard, password change for admin, and basic login/logout from the admin panel
## 0.2 : sprint 2
	RSS feed reader and settings, edit/add ceds profile and details, Support tickets for admin and notifications on slack, 
	mail and admin backend app, Journal/paper function.
	