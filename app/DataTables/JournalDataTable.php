<?php

namespace App\DataTables;

use App\Models\Journal;
use Yajra\DataTables\Services\DataTable;

class JournalDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($query){
                return '<a href="'.route('journal',['id'=>$query->id]).'" class="btn btn-xs btn-primary" ><i class="fa fa-edit"></i> Edit</a><button type="button" data-id="'.$query->id.'" class="btn-delete btn-xs btn btn-danger"><i class="fa fa-trash"></i></button>';
            })
            ->editColumn('author', function($query){
                $author ='';
                foreach (json_decode($query->author) as $auth){
                    $author .= '<span class="author-journal">'.$auth.'</span>';
                };
                return $author;
            })
            ->editColumn('title', function($query){
                return '<a href="'.route('journal',['id'=>$query->id]).'">'.$query->title.'</a>';
            })
            ->rawColumns(['title','author','is_local','action'])
            ->editColumn('created_at',function($query){
                return $query->created_at->diffForHumans();
            })
            ->editColumn('is_local', function($query){
                if ($query->is_local) {
                    return '<span class="label label-info">Stored</span>';
                }
                else {
                    return '<span class="label label-default">Remote</span>';
                };
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Journal $model)
    {
        return $model->newQuery()->select('id','title', 'author', 'is_local', 'created_at')->withCount('download');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title',
            'author',
            ['data' => 'is_local', 'name' => 'is_local', 'title' => 'Type'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Posted'],
            ['data' => 'download_count', 'name' => 'download_count', 'title' => 'Downloaded']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Journal_' . date('YmdHis');
    }
}
