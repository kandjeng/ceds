<?php

namespace App\DataTables;

use App\Models\TableData;
use Yajra\DataTables\Services\DataTable;

class EconDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->rawColumns(['stored','action'])
        ->addColumn('action', function($query){
            return '<a href="'.route('tabledata',['id'=>$query->id]).'" class="btn btn-xs btn-primary" ><i class="fa fa-edit"></i> Edit</a><button type="button" data-id="'.$query->id.'" data-name="'.$query->title.'" class="btn-delete btn-xs btn btn-danger"><i class="fa fa-trash"></i></button>';
            })
        ->editColumn('stored',function($query){
            $typedata = TableData::find($query->id)->details->pluck('title');
            $txt = '';
            foreach ($typedata as $type){
                $txt .= '<span class="label label-store label-info">'.$type.'</span>';
            };
            return $txt;
            })
        ->editColumn('created_at',function($query){
                return $query->created_at->diffForHumans();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TableData $model)
    {
        return $model->newQuery()->select('id', 'title', 'created_at')->withCount('download');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title',
            'stored' => ['orderable' => false, 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Added'],
            ['data' => 'download_count', 'name' => 'download_count', 'title' => 'DL']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Econ_' . date('YmdHis');
    }
}
