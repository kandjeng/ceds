<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Journal;
use App\DataTables\JournalDataTable;
use Storage;

class JournalController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all the journals/index.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(JournalDataTable $datatable) {
    	return $datatable->render('layouts.journal.index');
    }

    /**
     * Show the journal page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showjournal($id) {
        $journal = Journal::find($id);
    	return view('layouts.journal.journal',['journal'=>$journal]);
    }

    /**
     * Add a new journal.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request) {
    	$journal = new Journal;
    	$journal->title = $request->get('title');
    	$journal->author = json_encode(explode(',',$request->get('author')));
    	$journal->short = $request->get('short');
    	$journal->created_by=\Auth::user()->id;
        if ($journal->is_local = $request->get('is_local')) {
        	$path = $request->file('textfile')->store('journal','gasibu');
	    	$local = Storage::disk('gasibu')->getDriver()->getAdapter()->getPathPrefix();
	    	$journal->strand=pathinfo($path,PATHINFO_FILENAME);
        }
        else {
        	$journal->strand = $request->get('journal_url');
        };
    	if ($journal->save()){
    		return redirect()->back()->withSuccess('Journal Stored')->with('text',"Successfully stored the journal to database");
    	}
    	else {
    		return redirect()->back()->withWarning('Fail to store journal')->with('text',"Something's wrong while storing journal, please try again");
    	};
    }
    
    /**
     * Edit existing journal.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $journal = Journal::find($request->get('journal-id'));
        $journal->title=$request->get('title');
        $journal->author=json_encode(explode(',',$request->get('author')));
        $journal->short=$request->get('short');
        if ($journal->is_local) {
        	if ( $request->hasFile('textfile') || !$request->get('is_local') ) {
				Storage::disk('gasibu')->delete('journal/'.$journal->strand.'.pdf');
			};
        };
        if ($journal->is_local = $request->get('is_local')) {
        	if ( $request->hasFile('textfile') ){
        		$path = $request->file('textfile')->store('journal','gasibu');
				$local = Storage::disk('gasibu')->getDriver()->getAdapter()->getPathPrefix();
				$journal->strand=pathinfo($path,PATHINFO_FILENAME);
        	}
        }
        else {
        	$journal->strand = $request->get('journal_url');
        };
        if ($journal->save()){
            return redirect()->route('indexJournal')->withSuccess("Journal Updated")->with('text',"Successfully updated journal");
        }
        else {
            return redirect()->back()->withWarning('Fail to update')->with('text',"Something's wrong while updating the journal, please try again later");
        };
    }

    /**
     * Delete a journal.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request) {
        $journal = Journal::find($request->get('journal-id'));
        if ($journal->is_local){
        	Storage::disk('gasibu')->delete('journal/'.$journal->strand.'.pdf');
        };
        if ($journal->delete()){
            return redirect()->route('indexJournal')->withSuccess("Journal Deleted")->with('text',"Successfully deleted a journal");
        }
        else {
            return redirect()->route('indexJournal')->withWarning("Fail to delete")->with('text',"Something's wrong while deleting, please try again later");
        }
    }

}
