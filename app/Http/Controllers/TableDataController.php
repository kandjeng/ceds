<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TableData;
use App\Models\TableDataDetails;

class TableDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show/edit the data page
     *
     * @return \Illuminate\Http\Response
     */
    public function showdata($id) {
        $tabledata = TableData::find($id);
        $tree = get_category_tree();
        $categoryTree = [];
        $traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree,&$category){
            $cat=[];
            foreach ($categories as $categ) {
                $cat['id']=$categ['id'];
                $cat['title']='';
                $cat['top']= $depth==0?'first':'';
                for ($i=0; $i < $depth; $i++) { 
                    $cat['title'].= '&emsp;';
                };
                $cat['title'].=$categ['title'];
                if ($categ['title']!='UNCATEGORIZED') {
                    array_push($categoryTree, $cat);
                    if (count($categ['child'])>0) {
                        $traverse($categ['child'],$depth+1);
                    };
                };                
            };
        };
        $traverse($tree);
        $typedata = $tabledata->details->pluck('title');
        return view('layouts.data',['ecodata'=>$tabledata,'categories'=>$categoryTree,'typedata'=>$typedata,'tags'=>$tabledata->tags->implode('name',',')]);
    }

    /**
     * Add a new TableData.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request) {
    	$tabledata = TableData::create(['title' => $request->get('title'), 'short' => $request->get('short'), 'created_by' => \Auth::user()->id, 'category_id' => $request->get('cat_id')]);
    	$tabledata->tag($request->get('data_tags'));
        $path = $request->file('datafile')->getRealPath();
    	$parent = $tabledata->id;
        //try {
			$dataloaded = \Excel::load($path, function($reader) use ($parent) {
				$reader->each(function($sheet) use ($parent) {
					$datadetail = collect([]);
                    $col_title = $sheet->first()->keys();
                    $ymax=0;
                    $ymin=0;
                    foreach ($sheet as $row) {
                        $tmp = $row->values()->slice(1)->push($ymax)->push($ymin)->toArray();
                        $ymax = max($tmp);
                        $ymin = min($tmp);
						$datadetail->push($row->values());
					};
                    $tabledetail = TableDataDetails::create([
                        'title' => $sheet->getTitle(),
                        'table_id' => $parent,
                        'ymax' => $ymax,
                        'ymin' => $ymin,
                        'column_title'=> $col_title->toJson(),
                        'column_data' => $datadetail->toJson()
                    ]);
				});
			});
		//} catch (\Exception $ex) {
		//	TableDataDetails::where('table_id',$parent)->get()->each(function($detail){
		//		$detail->delete();
		//	});
		//	$tabledata->delete();
		//	return redirect()->back()->withWarning('Fail to store data')->with('text',"Something's wrong while storing data, please try again");
		//}
    	return redirect()->back()->withSuccess('data stored')->with('text','Successfully stored the data');
    }

    /**
     * Edit existing TableData.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $tabledata = TableData::find($request->get('data_id'));
        try {
            $tabledata->title = $request->get('title');
            $tabledata->category_id = $request->get('category_id');
            $tabledata->short = $request->get('short');
            $parent = $request->get('data_id');
            $tabledata->retag($request->get('data_tags'));
        
            if ($request->hasFile('datafile')) {
                $path = $request->file('datafile')->getRealPath();
                $olddetails = TableDataDetails::where('table_id',$tabledata->id)->get();
                \Excel::load($path, function($reader) use ($parent) {
                    $reader->each(function($sheet) use ($parent) {
                        $datadetail = collect([]);
                        $col_title = $sheet->first()->keys();
                        foreach ($sheet as $row) {
                            $datadetail->push($row->values());
                        };
                        $tabledetail = TableDataDetails::create([
                            'title' => $sheet->getTitle(),
                            'table_id' => $parent,
                            'column_title'=> $col_title->toJson(),
                            'column_data' => $datadetail->toJson()
                        ]);
                    });
                });
                $olddetails->each(function($detail){
                    $detail->delete();
                });    
            };
            $tabledata->save();
        } catch (\Exception $e){
            return redirect()->back()->withWarning('Fail to update')->with('text',"Something's wrong while updating the data, please try again later");    
        }
        return redirect()->route('category',['id'=>$request->get('category_id')])->withSuccess("Data Updated")->with('text',"Successfully updated data");
    }

    /**
     * Delete a TableData.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request) {
        $tabledata = TableData::find($request->get('data_id'));
        TableDataDetails::where('table_id',$tabledata->id)->get()->each(function($detail){
            $detail->delete();
        });
        $category = $tabledata->category;
        if ($tabledata->delete()){
            return redirect()->route('category',$category)->withSuccess("Data Deleted")->with('text',"Successfully deleted a data");
        }
        else {
            return redirect()->route('category',$category)->withWarning("Fail to delete")->with('text',"Something's wrong while deleting, please try again later");
        }
    }
    
    /**
     * get all available tags for a table data
     *
     * @return json
     */
    public function getalltags(){
        return response()->json(collect(TableData::allTags())->map(function($item){return ['name'=>$item];})->values());
    }

}
