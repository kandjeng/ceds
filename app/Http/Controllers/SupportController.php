<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\SupportDataTable;
use App\Models\Tickets;
use App\Models\TicketResponse;
use App\Notifications\SupportNote;
use App\Notifications\ResponseNote;
use App\Models\User;


class SupportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all the tickets.
     *
     * @return \Illuminate\Http\Response
     */
    public function all($status = 'open') {
        if ($status=='closed'){
            $tickets = Tickets::where('status','closed')->orderBy('created_at','desc')->paginate(8);
        }
        else{
            $tickets = Tickets::where('status','!=','closed')->orderBy('created_at','desc')->paginate(8);
            foreach(\Auth::user()->unreadNotifications as $notif){
                $tickets->find($notif->data['ticket_id'])->unread=1;
            }
        }
        
        $data = [
            'tickets'=>$tickets,
            'status'=>$status,
            'state' =>['open'=>'label-success','on progress'=>'label-warning'],
            'priority'=>[
                    ['label'=>'minor', 'class'=>'label-info'],
                    ['label'=>'medium', 'class'=>'label-warning'],
                    ['label'=>'urgent', 'class'=>'label-danger']]
            ];
        return view('layouts.support.index',$data);
    }

     /**
     * Show the individual ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function showticket(Tickets $ticket) {
        foreach (\Auth::user()->unreadNotifications as $notif){
            if ($notif->data['ticket_id']==$ticket->id){
                $notif->markAsRead();
                break;
            };
        };
        $data = [
            'ticket'=>$ticket,
            'status' => ['open'=>'label-success','on progress'=>'label-warning','closed'=>'label-default'],
            'priority' => [
                ['class'=> 'label-info','label'=>'Minor'],
                ['class'=> 'label-warning','label'=>'Medium'],
                ['class'=> 'label-danger','label'=>'URGENT !!']],
            'responses'=>$ticket->responses
        ];
        return view('layouts.support.ticket',$data);
    }

    /**
     * Add support ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function addticket(Request $request) {
        $ticket = new Tickets;
        $ticket->created_by = \Auth::user()->id;
        $ticket->token = rand_string(9);
        $ticket->title = $request->get('title');
        $ticket->priority = $request->get('priority');
        $ticket->message= $request->get('message');
        if ($ticket->save()){
            User::where('is_admin','1')->first()->notify(new SupportNote($ticket));
            return redirect()->back()->withSuccess('Support ticket sent')->with('text','Successfully send new support ticket');
        }
        else{
            return redirect()->back()->withWarning('Failed to send ticket')->with("text","Something's wrong while sending ticket, please try again");
        }
    }

    /**
     * Edit support ticket state
     *
     * @return \Illuminate\Http\Response
     */
    public function editticket(Request $request) {
        $ticket = Tickets::find($request->get('ticket_id'));
        $ticket->status = $request->get('status');
        $ticket->priority = $request->get('priority');
        if ($ticket->save()){
            return redirect()->back()->withSuccess('Ticket state updated')->with('text',"Successfully update support ticket's state");
        }
        else{
            return redirect()->back()->withWarning('Failed to update ticket')->with("text","Something's wrong while updating ticket, please try again");
        }
    }

     /**
     * delete a support ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteticket(Tickets $ticket) {
        $responses = $ticket->responses;
        foreach ($responses as $key => $response) {
            $response->delete();
        };
        $notifs=\Auth::user()->notifications->where('type','App\Notifications\SupportNote');
        foreach ($notifs as $key => $notif) {
            if ($notif->data['ticket_id']==$ticket->id){
                $notif->delete();break;
            }
        }
        if ($ticket->delete()){
            return redirect()->back()->withSuccess('Ticket Deleted')->with('text',"Successfully delete support ticket");
        }
        else{
            return redirect()->back()->withWarning('Failed to delete ticket')->with("text","Something's wrong while deleting ticket, please try again");
        }
    }

    /**
     * Add reply to the ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function addresponse(Request $request) {
        $response =  new TicketResponse;
        $response->ticket_id = $request->get('ticket_id');
        $response->created_by = \Auth::user()->id;
        $response->message = $request->get('message');
        if ($response->save()){
            if ($response->author->is_admin){
                $response->ticket->author->notify(new ResponseNote($response));
            }
            else{
                User::where('is_admin','1')->first()->notify(new ResponseNote($response));
            };
            return redirect()->back()->withSuccess('Reply Sent')->with('text','Reply Sent to this ticket');
        }
        else{
            return redirect()->back()->withWarning('Failed to reply')->with("text","Something's wrong while sending reply, please try again");
        }
    }
}
