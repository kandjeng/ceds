<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\CategoryData;
use App\DataTables\EconDataTable;
use App\DataTables\Scopes\EconDataScope;


class CategoryDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the add category page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function showaddcategory($id)
    {
    	$tree = get_category_tree();
    	$categoryTree = [];
    	array_push($categoryTree, ['id' => 0, 'title' => 'TOP LEVEL','top' => 'top']);
    	$traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree){
    		$cat=[];
    		foreach ($categories as $category_single) {
    			$cat['id']=$category_single['id'];
    			$cat['title']='';
    			$cat['top']= $depth==0?'first':'';
    			for ($i=0; $i < $depth; $i++) { 
    				$cat['title'].= '&emsp;';
    			};
    			$cat['title'].=$category_single['title'];
    			array_push($categoryTree, $cat);
    			if (count($category_single['child'])>0) {
    				$traverse($category_single['child'],$depth+1);
    			};
    		};
    	};
    	$traverse($tree);
        return view('layouts.addcategory',['current'=>$id,'category_tree'=>$categoryTree]);
    }

    /**
     * Show the edit category page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function showeditcategory(CategoryData $category){
        $tree = get_category_tree();
        $categoryTree = [];
        array_push($categoryTree, ['id' => 0, 'title' => 'TOP LEVEL','top' => 'top']);
        $traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree,&$category){
            $cat=[];
            foreach ($categories as $categ) {
                $cat['id']=$categ['id'];
                $cat['title']='';
                $cat['top']= $depth==0?'first':'';
                for ($i=0; $i < $depth; $i++) { 
                    $cat['title'].= '&emsp;';
                };
                $cat['title'].=$categ['title'];
                if ($cat['id']!=$category->id && $categ['title']!='UNCATEGORIZED') {
                    array_push($categoryTree, $cat);
                    if (count($categ['child'])>0) {
                        $traverse($categ['child'],$depth+1);
                    };
                };
                
            };
        };
        $traverse($tree);
        return view('layouts.editcategory',['current'=>$category,'category_tree'=>$categoryTree]);
    }

	/**
     * show category page
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function showcategory(EconDataTable $datatable, CategoryData $category){
        $children = $category->children;
        foreach ($children as $subcat){
            $subcat['created_by']=$subcat->author->name;
        }
    	return $datatable->addScope(new EconDataScope($category->id))->render('layouts.category',['subcategories' => $children, 'title' => $category->title, 'currentcategory'=>$category]);
    }
    
    /**
     * add New category
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function addcategory(Request $request){
    	$newCat = new CategoryData;
        $parent = $request->get('parent-id');
    	$newCat->parent_id = $parent;
    	$newCat->title = $request->get('title');   
    	$newCat->description = $request->get('description');
    	$newCat->created_by = Auth::user()->id;
    	if ($newCat->save()){
            if ($parent){
    	       return redirect()->route('category',['id' => $parent])->withSuccess("New Category Added !")->with("text","Sucessfully added new category");
            }
            else {
                return redirect()->route('home')->withSuccess("New Category Added !")->with("text","Sucessfully added new category");
            }

        }
        else{
            return redirect()->route('category',['id' => $parent])->withWarning("Warning !")->with("text","Failed to add new category"); 
        }
    }

    /**
     * Edit category
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function editcategory(Request $request){
        $Cat = CategoryData::find($request->get('current_id'));
        $Cat->parent_id = $request->get('parent-id');
        $Cat->title = $request->get('title');
        $Cat->description = $request->get('description');
        if ($Cat->save()){
            if ($Cat->parent_id){
                return redirect()->route('category',['category' => $Cat->parent])->withSuccess("Category Updated !")->with("text","Category updated sucessfully");
            }
            else {
                return redirect()->route('home')->withSuccess("Category Updated !")->with("text","Category updated sucessfully");
            }
        }
        else{
           return redirect()->route('category',['category' => $Cat->parent])->withWarning("Warning !")->with("text","Failed to update category"); 
        }
    }

    /**
     * Delete a category
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function deletecategory(Request $request){
        $category = CategoryData::find($request->get('cat_id'));
        $parent = $category->parent_id;
        foreach ($category->children as $child){
            $child->parent_id = CategoryData::where('title','UNCATEGORIZED')->select('id')->first()->id;
            $child->save();
        };
        if ($category->delete()){
            if ($parent){
                return redirect()->route('category',['category'=>$parent])->withSuccess('Category Deleted!')->with('text','Sucessfully delete a category');
            }
            else {
                return redirect()->route('home')->withSuccess('Category Deleted!')->with('text','Sucessfully delete a category');
            }
        }
        else{
            return redirect()->route('category',['category'=>$parent])->withWarning('Delete Fail!')->with('text', 'failed to delete category');
        };

        //parse subcategory and data, move it to uncategorized        
    }
}
