<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Journal;
use App\Models\JournalDownload;
use App\Models\TableData;
use App\Models\TableDataDownload;
use Feeds;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recentDataDl = TableDataDownload::whereDate('created_at','>',Carbon::now()->subDays(7))->get()->groupBy(function($date){return Carbon::parse($date->created_at)->format('Y-m-d');})->take(8);
        $recentJournalDl = JournalDownload::whereDate('created_at','>',Carbon::now()->subDays(7))->get()->groupBy(function($date){return Carbon::parse($date->created_at)->format('Y-m-d');})->take(8);
        $recentDL = array();
        for ($i=7; $i >= 0 ; $i--) { 
            $idx = Carbon::now()->subDays($i)->format('Y-m-d');
            $recentDL[$idx]=[
                'data' => (isset($recentDataDl[$idx])?$recentDataDl[$idx]->count():collect([])),
                'journal' => (isset($recentJournalDl[$idx])?$recentJournalDl[$idx]->count():collect([])),
            ];
        }
        $stats=[
            'data_total'=> TableData::count(),
            'data_total_dl'=> TableDataDownload::count(),
            'data_popular'=> TableData::select('id','title')->withCount('download')->orderByDesc('download_count')->take(5)->get(),
            'data_latest'=> TableData::select('id','title','created_at')->orderByDesc('created_at')->take(5)->get(),
            'data_recent_downloaded'=> TableDataDownload::join('table_datas', 'table_id', '=', 'table_datas.id')
                                        ->select('table_data_downloads.identifier', 'table_data_downloads.created_at', 'title')
                                        ->orderByDesc('table_data_downloads.created_at')->take(5)
                                        ->get(),
            'journal_recent_download' =>JournalDownload::join('journals', 'journal_id', '=', 'journals.id')
                                        ->select('journal_downloads.identifier', 'journal_downloads.created_at', 'title')
                                        ->orderByDesc('journal_downloads.created_at')->take(5)
                                        ->get(),
            'journal_total'=> Journal::count(),
            'journal_total_dl'=> JournalDownload::count(),
            'journal_popular'=> Journal::select('id','title')->withCount('download')->orderByDesc('download_count')->take(5)->get(),
            'journal_latest'=> Journal::select('id','title','created_at')->orderByDesc('created_at')->take(5)->get()
        ];
        $feeds = Feeds::make(get_app_settings('feeds_url'),get_app_settings('feeds_number'));
        $data = array(
            'site' => get_app_settings('feeds_url'),
            'title' => $feeds->get_title(),
            'permalink' => $feeds->get_permalink(),
            'items' => $feeds->get_items(),
            'feednum' => get_app_settings('feeds_number'),
            'stats'=>$stats,
            'recent_data' => $recentDL
        );
        return view('layouts.home',$data);
    }

    /**
     * Show the change password page
     *
     * @return \Illuminate\Http\Response
     */
    public function showchangepass()
    {
        return view('layouts.changepass');
    }

    /**
     * Perform the signout function
     *
     * @return \Illuminate\Http\Response
     */
    public function SignOut(){
        \Auth::logout();
        return redirect()->route('home');
    }

    /**
     * change the password
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function changepass(Request $request){
 
        if (!(\Hash::check($request->get('current-password'), \Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->withWarning("Something's Wrong")->with("text","Your current password does not matches with the password you provided. Please try again.");
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->withWarning("Something's Wrong")->with("text","New Password cannot be same as your current password. Please choose a different password.");
        }
        
        $rules = array(
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        );
        
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->back()->withWarning("Something's Wrong")->with("text",$messages);
        }

        $user = \Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
 
        return redirect()->route('home')->withSuccess("Password changed !")->with("text","You have change your password");
 
    }
}
