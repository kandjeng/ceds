<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CEDSProfile;
use App\Models\ProfileDetails;
use Storage;

class CEDSProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the edit CEDS Profile brief description
     *
     * @return \Illuminate\Http\Response
     */
    public function showeditbrief() {
        $id = CEDSProfile::where('short','description')->pluck('detail_id')->first();
    	return view('layouts.profile.brief',['id'=>$id, 'data' => ProfileDetails::find($id)]);
    }

    /**
     * Show the edit CEDS Service page
     *
     * @return \Illuminate\Http\Response
     */
    public function showeditservice() {
        $data = [
            'id' => CEDSProfile::where('short','service')->pluck('detail_id')->first(),
            'data' => CEDSProfile::where('short','service')->first()->details
        ];
        return view('layouts.profile.service',$data);
    }

    /**
     * Show the edit CEDS Members page
     *
     * @return \Illuminate\Http\Response
     */
    public function showeditmember() {
        $data = [
            'id' => CEDSProfile::where('short','member')->pluck('detail_id')->first(),
            'data' => CEDSProfile::where('short','member')->first()->details
        ];
        return view('layouts.profile.member',$data);
    }

    /**
     * Show the edit CEDS Contact page
     *
     * @return \Illuminate\Http\Response
     */
    public function showeditcontact() {
        $data = [
            'id' => CEDSProfile::where('short','contact')->pluck('detail_id')->first(),
            'data' => CEDSProfile::where('short','contact')->first()->details
        ];
        return view('layouts.profile.contact',$data);
    }

    /**
     * Edit Profile Details
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function editdetail(Request $request){
        $detail = ProfileDetails::find($request->get('detail-id'));
        $detail->title = $request->get('title');
        $detail->content = $request->get('content');
        if ($detail->save()){
            return redirect()->back()->withSuccess('Success')->with('text', 'Profile has been saved');
        }
        else {
            return redirect()->back()->withWarning('Failed to save')->with("text","something wrong, please try again");
        };
    }

    /**
     * Edit Profile Details for ajax response
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function editdetailx(Request $request){
        if ($request->get('detail-id')==0){
            $detail= new ProfileDetails;
            $detail->parent_id = $request->get('parent');
            $detail->type='text';
        }
        else{
            $detail = ProfileDetails::find($request->get('detail-id'));
        };
        $detail->title = $request->get('title');
        $detail->content = $request->get('content');
        if ($detail->save()){
            return response()->json(['type' => 'success', 'title'=>'Detail saved',"text"=>"sucessfully updated detail profile"]);
        }
        else {
            return response()->json(['type' => 'warning', 'title'=>'Failed to save',"text"=>'something wrong, please try again']);
        };
    }

    /**
     * Delete Profile Details for ajax response
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function deletedetailx(Request $request){
        $detail = ProfileDetails::find($request->get('detailid'));
        if ($detail->delete()){
            return response()->json(['type' => 'success', 'title'=>'Detail deleted',"text"=>"sucessfully deleted detail profile"]);
        }
        else {
            return response()->json(['type' => 'warning', 'title'=>'Failed to delete',"text"=>'something wrong, please try again']);
        };
    }

    /**
     * Edit member for ajax response
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function editmember(Request $request){
        $success = true;
        $member = ProfileDetails::find($request->get('detail-id'));
        $details = $member->children;
        $member->content=$request->get('name');
        foreach ($details as $key => $detail) {
            if ($detail->type == 'text'){
                $detail->content=$request->get($detail->title);
                if (!$detail->save()){
                    $success=false;
                    break;
                }
            }
            elseif ($detail->type == 'image'){
                if ($request->hasFile('photo')){
                    Storage::disk('cpcopy')->delete($detail->content);
                    Storage::disk('cimandiri')->delete($detail->content);
                    $request->file('photo')->store('members/images','cimandiri');
                    $detail->content = $request->file('photo')->store('members/images','cpcopy');
                    
                    if (!$detail->save()){
                        $success=false;
                        break;
                    };
                };

            }
        };
        $success &= ($member->save())?true:false;
        if ($success){
            return response()->json(['type' => 'success', 'title'=>'Member saved',"text"=>"Member's info updated successfully"],200);
        }
        else {
            return response()->json(['type' => 'warning', 'title'=>'Failed to save',"text"=>"Something's wrong while updating member's info"],200);
        };
    }

    /**
     * add member for ajax response
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function addmember(Request $request){
        $titledetail = ['role','bio','email','picture'];
        $member = new ProfileDetails;
        $member->title='name';
        $member->type='repeater';
        $member->parent_id= CEDSProfile::where('short','member')->first()->detail_id;
        $member->content=$request->get('name');
        $success = ($member->save())?true:false;
        $parent=$member->id;
        foreach ($titledetail as $key => $detail) {
            $info = new ProfileDetails;
            $info->parent_id = $parent;
            $info->title=$detail;
            if ($detail!='picture'){
                $info->type='text';
                $info->content=$request->get($detail);
            }
            else{
                $info->type= 'image';
                if ($request->hasFile('photo')){
                    $request->file('photo')->store('members/images','cimandiri');
                    $info->content = $request->file('photo')->store('members/images','cpcopy');
                }
                else{
                    $name= 'members/images' + uniqid() + '.png';
                    Storage::disk('cpcopy')->copy('members/images/user.png', $name);
                    //Storage::disk('cimandiri')->put($name,Storage::disk('cpcopy')->get($name));
                    $info->content = $name;
                }
            }
            if (!$info->save()){
                $success=false;
                break;
            }
        }
        if ($success){
            return response()->json(['type' => 'success', 'title'=>'Member saved',"text"=>"New Member Added"],200);
        }
        else {
            return response()->json(['type' => 'warning', 'title'=>'Failed to save',"text"=>"Something's wrong while adding new member"],200);
        };
    }
    /**
     * delete a member for ajax response
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function deletemember(Request $request){
        $detail = ProfileDetails::find($request->get('detailid'));
        $children = $detail->children;
        foreach ($children as $key => $child) {
            if ($child->type == 'image'){
                Storage::disk('cimandiri')->delete($child->content);
                Storage::disk('cpcopy')->delete($child->content);
            };
            $child->delete();
        };
        if ($detail->delete()){
            return response()->json(['type' => 'success', 'title'=>'Detail deleted',"text"=>"sucessfully deleted detail profile"]);
        }
        else {
            return response()->json(['type' => 'warning', 'title'=>'Failed to delete',"text"=>"Something's wrong while deleting member"]);
        };
    }

    /**
     * get Profile Details
     *
     * @param $id
     * @return json
     */
    public function getdetail($id){
        return response()->json(ProfileDetails::find($id));
    }

    /**
     * get details from the child detail
     *
     * @param $id
     * @return json
     */
    public function getchilddetail($id){
        return response()->json(ProfileDetails::find($id)->children);
    }

    /**
     * get all ceds members via ajax call
     *
     * @return json
     */
    public static function getmembers(){
        $temp= CEDSProfile::where('short','member')->first()->details->children->toArray();
        $members = [];
        foreach ($temp as $key => $member) {
            $members[$key]=$member;
            $details=ProfileDetails::find($member['id'])->children->toArray();
            foreach ($details as $detail) {
                $members[$key][$detail['title']]=$detail['content'];
            }
            
        }
        return response()->json($members);
    }

    /**
     * get all ceds member detail via ajax call
     *
     * @return json
     */
    public function getmemberdetail($id){
        $member = ProfileDetails::find($id)->toArray();
        $details = ProfileDetails::find($id)->children->toArray();
        foreach ($details as $key => $detail) {
            $member[$detail['title']]=$detail['content'];
        };
        return response()->json($member);
    }
}
