<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoryData;
use App\Models\TableData;
use App\Models\TableDataDetails;
use App\Models\TableDataDownload;
use App\Models\Journal;
use App\Models\JournalDownload;
use App\Models\CEDSProfile;
use App\Models\ProfileDetails;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Controllers\CEDSProfileController;
use Ramsey\Uuid\Uuid;
use Feeds;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class StruggleController extends Controller
{
    /**
     * List the main category data
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexmaincategory()
    {
    	return CategoryData::where('parent_id',0)->where('id','>',1)->select('id','title')->get();
    }

    /**
     * List the category data and sub-category
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function showcategory($id)
    {
    	$cat = CategoryData::find($id);
    	$tables = $cat->ecodata->map(function ($table) {return collect($table->toArray())->only(['id', 'title'])->all();});
    	return ['title' => $cat->title,'short' => $cat->description,'subcat'=> CategoryData::where('parent_id',$id)->select('id','title')->get(),'data' => $tables];
    }

    /**
     * get the data from backend
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function showdata($id)
    {
        $tab = TableData::find($id);
        $details = \DB::table('table_data_details')->where('table_id',$id)->select('id','title')->get();
        $tabData = \Cache::remember('tabdet-'.$details[0]->id, 15, function() use($details) {
            return TableDataDetails::find($details[0]->id);
        });
        $label = json_decode($tabData->column_title);
        $col_data = json_decode($tabData->column_data);
        $data = array_slice($col_data,0,10);
        $dataDetail = [];
        for ($i=1; $i < count($data[0]) ; $i++) { 
            $dataDetail[] = [
                'data' => array_pluck($data,$i),
                'label' => $label[$i]
            ];
        };
    	return [
            'name' => $tab->title,
    		'title' => $tab->title,
            'short' => $tab->short,
            'details' => $details,
            'data_part' => 0,
            'nextAvail' => (count($col_data)>10)?1:0,
    		'sampledata' => [
                'ymin' => $tabData->ymin,
                'ymax' => $tabData->ymax,
                'bar_title' => array_pluck($data,0),
                'bar_data' => $dataDetail
            ]
    	];
    }
    
    /**
     * get the tabel details
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getdetail(Request $request)
    {
        $id = $request->get('id');
        $part = $request->get('seq');
        $detail = \Cache::remember('tabdet-'.$id, 15, function() use($id) {
            return TableDataDetails::find($id);
        });
        $label = json_decode($detail->column_title);
        $col_data = json_decode($detail->column_data);
        $data = array_slice($col_data,10*$part,10);
        $dataDetail = [];
        for ($i=1; $i < count($data[0]) ; $i++) { 
            $dataDetail[] = [
                'data' => array_pluck($data,$i),
                'label' => $label[$i]
            ];
        };
    	return [
            "id" => $id,
            "title" => $detail->title,
            'data_part' => $part,
            "nextAvail" => (count($col_data) > (10*($part+1)))?1:0,
            "details"=>[
                'ymin' => $detail->ymin,
                'ymax' => $detail->ymax,
                'bar_title' => array_pluck($data,0),
                'bar_data' => $dataDetail
            ]
        ];
    }

    /**
     * get multiple data from backend
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function showsetdata(Request $request)
    {
        $set = explode(',', $request->get('data'));
        $sample = collect([]);
        $title = collect([]);
        $dataset = \Cache::remember('setdet-'.$request->get('data'), 15, function() use($set){
            $title = collect([]);
            $sample = collect([]);
            foreach ($set as $key=>$id) {
                $detail = \Cache::remember('tabdet-'.$id, 15, function() use($id){
                    return TableDataDetails::find($id);
                });
                $title->push($detail->parent->title.' - '.$detail->title);
                $data = json_decode($detail->column_data);
                $label = json_decode($detail->column_title);
                $dataDetail = collect([]);
                for ($i=1; $i < count($data[0]) ; $i++) { 
                    $dataDetail->push([
                        'data' => array_pluck($data,$i),
                        'label' => $label[$i]
                    ]);
                };
                $sample = $sample->concat($dataDetail);
                if (!$key){
                    $name = $detail->title;
                    $ymin = $detail->ymin;
                    $ymax = $detail->ymax;
                    $bar_title = array_pluck($data,0);
                } else {
                    $ymin = min([$detail->ymin,$ymin]);
                    $ymax = max([$detail->ymax,$ymax]);
                };
            };
            return [
                "name" => $name,
                "title" => $title,
                "ymin" => $ymin,
                "ymax" => $ymax,
                "bar_title" => $bar_title,
                "bar_data" => $sample
            ];
        });
        
        $part = $request->get('seq');
        return [
            "name" => $dataset["name"],
            "title" => $dataset["title"],
            "nextAvail" => (count($dataset["bar_title"]) > (10*($part+1)))?1:0,
            "data_part" => $part,
            "ymin" => $dataset["ymin"],
            "ymax" => $dataset["ymax"],
            "bar_title" => array_slice($dataset["bar_title"],$part*10,10),
            "bar_data" => array_map(function($dadat)use($part){
                    return ["data" => array_slice($dadat["data"],$part*10,10), "label" => $dadat["label"]];
                },$dataset["bar_data"]->toArray())
        ];
    }

    /**
     * List all the journal stored
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexjournal()
    {
        $list = Journal::select('id','title','author')->get()->sortBy('id');
        foreach ($list as $journal){
            $journal['author']=implode(', ',json_decode($journal['author']));
        };
        return $list;
    }

    /**
     * get the journal from backend
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function showjournal($id)
    {
        $journal = Journal::find($id);
        $journal['author'] = implode(', ', json_decode($journal['author']));
        return $journal;
    }

    /**
     * List all articles from feeds
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexfeeds()
    {
        $feeds = \Feeds::make(get_app_settings('feeds_url'),get_app_settings('feeds_number'));
        $data = collect([]);
        foreach ($feeds->get_items() as $item){
            $data->push([
                'permalink' => urlencode($item->get_permalink()),
                'title' => $item->get_title(),
                'date' => $item->get_date()
            ]);
        }
        return $data;
    }

    /**
     * feth article from ceds website
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function fetcharticle(Request $request)
    {
        $client = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $client->setClient($guzzleClient);
        $crawler = $client->request('GET', urldecode($request->get('address')));
        $crawler->filter('table.pagenav')
            ->each(function($nodes){
                foreach ($nodes as $node) {
                    $node->parentNode->removeChild($node);
                };
            });
        return json_encode($crawler->filter('div.article-content')->html());
    }
    
    /**
     * get related table data, for combining data
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function showrelated(Request $request)
    {
        $tables = TableDataDetails::where('title',$request->get('tab_name'))->get();
        $tableSend = collect([]);
        $ids = explode(',',$request->get('tab_id'));
        $tabComp = TableDataDetails::find($ids[0]);
        foreach ($tables as $table) {
            if ($tabComp->parent->tagList==$table->parent->tagList){
                $tableSend->push(['id'=>$table->id,'title'=>$table->parent->title.' - '.$table->title]);
            }
        }

        return $tableSend->whereNotIn('id',$ids)->values();
    }

    /**
     * get related table data, for combining data
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function genjournal(Request $request)
    {
        $journal = Journal::find($request->get('id'));
        $identifier = Uuid::uuid1();
        if($journal->is_local){
            $name=$journal->strand.'.pdf';
            $storeAs = trim_content(preg_replace('/[^A-Za-z0-9-\s.,_]+/', '',$journal->title),35,'').'.pdf';
            if (!\Storage::exists('public/exports/journal/'.$storeAs)){
                \Storage::put('public/exports/journal/'.$storeAs, \Storage::disk('gasibu')->get('journal/'.$name));
            };
            $path = $identifier->getHex();
        } else {
            $path = $journal->strand;
        };
        $journalDL = JournalDownload::create([
            'identifier'=>$identifier->getHex(),
            'patron' => $request->get('device_id'),
            'filename' => $storeAs,
            'journal_id'=>$journal->id]);
        return ['location' => $journal->is_local, 'identifier' => $path];        
    }

    /**
     * get Journal Location
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function journalloc($id)
    {
        return Journal::find($id)->is_local;
    }
    /**
     * get CEDS profile data
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function cedsprofile()
    {
        $description = CEDSProfile::where('short','description')->first()->details;
        $service = CEDSProfile::where('short','service')->first()->details;
        $service['children'] = ProfileDetails::find($service->id)->children;
        $member = CEDSProfile::where('short','member')->first()->details;
        $member['children'] = CEDSProfileController::getmembers();
        $cedsprofile = Array(
            'description' => $description,
            'service' => $service,
            'member' => $member
        );
        return response()->json($cedsprofile);
    }
    
    /**
     * get CEDS Contact
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function cedscontact()
    {
        $contact = CEDSProfile::where('short','contact')->first()->details;
        $contact['children'] = ProfileDetails::find($contact->id)->children;
        return $contact;
    }

    /**
     * download file as pdf or xls
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getsingle(Request $request){
        $format = $request->get('format');
        $IDs = explode(',',$request->get('detail_id'));
        $dataset = \Cache::remember('saveset-'.$request->get('detail_id'), 15, function() use($IDs){
            foreach ($IDs as $key=>$id){
                $detail = \Cache::remember('tabdet-'.$id, 15, function() use($id){
                    return TableDataDetails::find($id);
                });
                if ($key==0) {
                    $tablecontent = json_decode($detail->column_data,true);
                    $tabletitle = json_decode($detail->column_title,true);
                    $sheet = $detail->title;
                    $title = $detail->parent->title.'-'.$sheet;
                    $tableIds = array($detail->parent->id);
                } else {
                    foreach (json_decode($detail->column_title,true) as $idx => $coltitle) {
                        if ($idx) {
                            $tabletitle[] = $coltitle;
                        }
                    };
                    $tableIds[] = $detail->parent->id;
                    $tablecontentCol = count($tablecontent[0]);
                    $tabcomp= json_decode($detail->column_data,true);
                    $tabcompCol = count($tabcomp[0]);
                    $hay = array_column($tabcomp,0);
                    foreach ($tablecontent as $tidx => $content){
                        $result = array_search($content[0],$hay);
                        if ( $result || $content[0]==$hay[0]){
                            $tablecontent[$tidx]=array_merge($content,array_slice($tabcomp[$result],1));
                            unset($hay[$result]);
                        } else {
                            for ($i=1; $i < $tabcompCol ; $i++) { 
                                array_push($tablecontent[$tidx],'-');
                            }
                        };
                    };
                    foreach ($hay as $ihay => $overflow){
                        $temp = array($overflow);
                        for ($i=1; $i < $tablecontentCol; $i++) { 
                            $temp[] = '-';
                        };
                        array_push($tablecontent,array_merge($temp,array_slice($tabcomp[$ihay],1)));
                    }
                };
                
            };
            return [
                'title' => count($IDs)>1?$title.'_combined':$title,
                'sheet' => count($IDs)>1?$sheet.'_combined':$sheet,
                'tableids' => $tableIds,
                'tabtitle' => $tabletitle, 
                'tabcontent' => $tablecontent
            ];
        });
        $identifier = Uuid::uuid1();
        
        try {
            $file = \Excel::create($dataset['title'],function($spreadsheet)use($dataset){
                array_unshift($dataset['tabcontent'],$dataset['tabtitle']);
                $spreadsheet->sheet($dataset['sheet'],function($sheet)use($dataset){
                    $alphabet = range('A', 'Z');
                    $sheet->fromArray($dataset['tabcontent'], null, 'A1', false,false);
                    $sheet->cells('A1:'.$alphabet[count($dataset['tabtitle'])-1].'1', function($cells) {
                        $cells->setFontSize(12);
                        $cells->setFontWeight('bold');
                    });
                });
            });
            switch ($format){
                case 'xls':
                    $file->store('xlsx',storage_path('app/public/exports/datas'));
                    break;
                case 'pdf':
                    $file->store('pdf',storage_path('app/public/exports/datas'));
                    break;
            };
            foreach ($dataset['tableids'] as $tabid){
                $log[] = TableDataDownload::create(['identifier'=>$identifier->getHex(),'patron'=>$request->get('device_id'),'filename'=>$dataset['title'].(($format=='xls')?'.xlsx':'.pdf'),'table_id'=>$tabid]);
            }
            return response()->json(['status'=>'success','status_code'=>200,'message'=>"data exported",'data'=>['filetype'=>$format,'identifier'=>$identifier->getHex()]]);
        } catch(Exception $e){
            foreach ($log as $sids){  
                TableDataDownload::find($sids)->delete();
            }
            return response()->json(['status'=>'error','status_code'=>500,'message'=>"error while downloading data"]);
        }
    }

    /**
     * download file as pdf or xls
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function fetcher($identifier, Request $request){
        if ($request->get('type')=='xls' || $request->get('type')=='pdf'){
            $file = storage_path('app/public/exports/datas/'.TableDataDownload::where('identifier',$identifier)->first()->filename);            
        }
        elseif ($request->get('type')=='journal'){
            $file = storage_path('app/public/exports/journal/'.JournalDownload::where('identifier',$identifier)->first()->filename);
        }
        if (!\File::exists($file))
            return [];
        else
            return response()->download($file)->deleteFileAfterSend(true);
    }

    /**
     * display search query
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $key = $request->get('keyword');
        $limit = $request->get('limit') ?: 4;
        $arts = function($count = -1)use($key){
            $url = Feeds::make(get_app_settings('feeds_url'));
            $feeds = collect([]);
            $num = 0;
            foreach ($url->get_items() as $item) {
                $res = stripos($item->get_title(),$key);
                if ($res!==false){
                    $num++;
                    if ($feeds->count() < $count || $count==-1){
                        $feeds->push(collect([
                            'title'=>$item->get_title(),
                            'permalink'=>$item->get_permalink(),
                            'date' => $item->get_date()
                        ]));
                    };
                };
            };
            return ['items'=>$feeds,'found'=>$num];
        };
        $dats = function($count = -1)use($key){
            $tabdat = TableData::select('id','title')->where('title','LIKE','%'.$key.'%');
            $num = $tabdat->count();
            return ['items'=>($count!=-1)? $tabdat->take($count)->get():$tabdat->get(),'found'=>$num];
        };
        $jous = function($count =-1)use($key){
            $jlist = Journal::select('id','title','author')->where('title','LIKE','%'.$key.'%')->orWhere('author','LIKE','%'.$key.'%');
            $num = $jlist->count();
            $listjo = ($count!=-1) ? $jlist->take($count)->get(): $jlist->get();
            return [
                'items'=> $listjo->map(function($item,$index){
                        return [
                            'id' => $item->id,
                            'author' => implode(',',json_decode($item->author,true)),
                            'title' => $item->title,
                        ];
                    }),
                'found' => $num
                ];
        };
        switch ($request->get('in')) {
            case 'article':
                return response()->json([
                    'status'=>"success",'status_code'=>200,'message'=>'query processed',
                    'result'=> ['articles'=>$arts(-1)]
                ]);
                break;
            case 'journal':
                return response()->json([
                    'status'=>"success",'status_code'=>200,'message'=>'query processed',
                    'result'=> ['journals'=>$jous(-1)]
                ]);
                break;
            case 'data':
                return response()->json([
                    'status'=>"success",'status_code'=>200,'message'=>'query processed',
                    'result'=> ['datas'=>$dats(-1)]
                ]);
                break;
            case 'all':
                return response()->json([
                    'status' => 'success',
                    'status_code' => 200,
                    'message' => 'query_processed',
                    'result' => [
                        'articles' => $arts($limit),
                        'journals' => $jous($limit),
                        'datas' => $dats($limit),
                    ]
                ]);
                break;
        }
    }
    /**
     * home screen data
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function homereq(Request $request){
        $device= $request->get('device_id');
        $popData = TableData::select('table_datas.id','table_datas.title','category_id','category_datas.title as category_title')
            ->withCount('download')->orderByDesc('download_count')->join('category_datas','category_id','=','category_datas.id')->take(6)->get()
            ->map(function($item,$key){
                return array_merge($item->toArray(),['ancestor'=>CategoryData::find($item->category_id)->ancestor()]);
            });
        $url = Feeds::make(get_app_settings('feeds_url'),5);
        $feeds = collect([]);
        foreach ($url->get_items() as $key=>$item) {
            $feeds->push([
                'title' =>$item->get_title(),
                'permalink' => $item->get_permalink(),
                'date' => $item->get_date()
            ]);
            if ($key>=4)
                break;
        };
        return response()->json([
            'status' => 'success',
            'status_code' =>200,
            'message' => 'home data retrieved',
            'data' => [
                'pop_data' => $popData,
                'pop_journal' => Journal::select('id','title','author')->withCount('download')->orderByDesc('download_count')->take(5)->get()->map(function($item){
                        return [
                            'id' => $item->id,
                            'title' => $item->title,
                            'author' => implode(',',json_decode($item->author,true))
                        ];
                    }),
                'articles' => $feeds
            ]
        ]);
    }
}