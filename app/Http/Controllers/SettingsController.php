<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the rss feed setting page
     *
     * @return \Illuminate\Http\Response
     */
    public function showRSSsettings(){
        $feeds = ['feeds_url'=>get_app_settings('feeds_url'),'feeds_number'=>get_app_settings('feeds_number')];
        return view('layouts.settingrss',$feeds);
    }

    /**
     * update rss feed settings
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRSS(Request $request){
        if (update_app_settings("feeds_url",$request->get('feeds-url')) && update_app_settings("feeds_number",$request->get('feeds-number'))){
            return redirect()->route('home')->withSuccess("Settings updated")->with("text", "New RSS settings applied, the change will take effect in 5 minutes");
        }
        else{
            return redirect()->route('home')->withWarning("Failed to save settings")->with("text", "something went wrong, please try again later");
        }
    }
}
