<?php namespace App\Http\ViewComposers;

use Illuminate\View\View;

class SidebarView
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
    	$view->with('unreadNum',count(\Auth::user()->unreadNotifications));
        
    }
}