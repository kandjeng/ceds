<?php
use App\Settings;

if (!function_exists('get_app_settings')) {
    /**
     * Get the apps setting value from the specific type
     *
     * @return String
     */
    function get_app_settings($option_key) {
    	return Settings::where('option_key', $option_key)->first()->option_value;
    }
}
if (!function_exists('update_app_settings')) {
	/**
     * Update the specific setting on settings table
     *
     * @return boolean
     */
    function update_app_settings($option_key,$option_value) {
    	$setting = Settings::where('option_key',$option_key)->first();
    	if ($setting===null){
    		return false;
    	}
    	else{
    		$setting->option_value = $option_value;
    		return $setting->save();
    	}
    }
}
if (!function_exists('rand_string')) {
    /**
     * Update the specific setting on settings table
     *
     * @return boolean
     */
    function rand_string($length) {
        $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $char = str_shuffle($char);
        for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
            $rand .= $char{mt_rand(0, $l)};
        }
        return $rand;
    }
}
if (!function_exists('trim_content')) {
    /**
     * @param mixed
     */
    function trim_content($strg, $n = 500, $end_char = '&#8230;')
    {
        $str = strip_tags($strg);
        if (strlen($str) < $n){
            return $str;
        };
        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));
        if (strlen($str) <= $n){
            return $str;
        };
        $out = "";
        foreach (explode(' ', trim($str)) as $val){
            $out .= $val.' ';
            if (strlen($out) >= $n){
                $out = trim($out);
                return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
            }
        }
     }
}