<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\SidebarView;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('includes.sidebar', SidebarView::class);
        view()->composer('includes.topbar', function($view){
            $view->with('unreadNum',count(\Auth::user()->unreadNotifications));
            $view->with('notifs',\Auth::user()->unreadNotifications->take(5));
        });
    }
}