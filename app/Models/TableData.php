<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentTaggable\Taggable;

class TableData extends Model
{
    use Taggable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','short','created_by','category_id'];

    /**
     * get the user account uploaded the data
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploader()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * get the category of this Data
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(CategoryData::class, 'category_id');
    }

    /**
     * get the data details of this table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function details()
    {
        return $this->hasMany(TableDataDetails::class, 'table_id');
    }

    /**
     * get the downloads of this table
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function download()
    {
        return $this->hasMany(TableDataDownload::class, 'table_id');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTableData($query, $tabledata)
    {
        return $query->where('table_id', $tabledata);
    }
}
