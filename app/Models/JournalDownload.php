<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JournalDownload extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['identifier','patron','filename','journal_id'];

    /**
     * the journal associated with this download data
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function journal()
    {
        return $this->belongsTo(Journal::class, 'journal_id');
    }
}
