<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TableDataDownload extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['identifier','patron','filename','table_id'];

    /**
     * the journal associated with this download data
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tabledata()
    {
        return $this->belongsTo(TableData::class, 'table_id');
    }
}
