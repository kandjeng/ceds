<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class CategoryData extends Model
{
    /**
     * Relationship with the parent category
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(CategoryData::class, 'parent_id');
    }

    /**
     * Relationship with the child category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(CategoryData::class, 'parent_id');
    }

    /**
     * Get all Data from this category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ecodata()
    {
        return $this->hasMany(TableData::class, 'category_id');
    }

    /**
     * get the user account created the category
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * get ancestor parent
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ancestor(){
        if ($this->parent_id == 0)
            return $this->id;
        else 
            return $this->parent->ancestor();
    }
}
