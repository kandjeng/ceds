<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileDetails extends Model
{
    /**
     * relationship with the parent profile details
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(ProfileDetails::class, 'parent_id');
    }
    
    /**
     * relationship with all the child details 
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(ProfileDetails::class, 'parent_id');
    }

    /**
     * Relationship with the main CEDS profile section
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expanding()
    {
        return $this->hasOne(CEDSProfile::class, 'detail_id');
    }
}
