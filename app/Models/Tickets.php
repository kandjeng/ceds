<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use DB;

class Tickets extends Model
{
    
    /**
     * get the user account created the category
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Relationship with the responses for the ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function responses()
    {
        return $this->hasMany(TicketResponse::class, 'ticket_id');
    }

    /**
     * Get the read status by the responses
     *
     * @return boolean
     */
    public function isread()
    {
    	$status= true;
    	$responses = DB::table('ticket_responses')->where('ticket_id',$this->id)->select('is_read')->get();
    	foreach ($responses as $response) {
    		$status &= $response->is_read;
    	}
        return $status;
    }

    public function getPriorityReading(){
        $trans = ['Minor Problem', 'Medium', 'URGENT !!'];
        return $trans[$this->priority];
    }
}
