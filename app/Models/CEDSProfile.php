<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CEDSProfile extends Model
{
    /**
     * get the details from ceds profile section
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->belongsTo(ProfileDetails::class, 'detail_id');
    }
}
