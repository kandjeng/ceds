<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TableDataDetails extends Model
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','table_id','ymax','ymin','column_sample','column_title','column_data'];
    
    /**
     * the journal associated with this download data
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(TableData::class, 'table_id');
    }
}
