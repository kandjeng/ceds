<?php

namespace App\Notifications;

use App\Models\TicketResponse;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class ResponseNote extends Notification
{
    use Queueable;

    protected $response;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(TicketResponse $response)
    {
        $this->response = $response;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->is_admin? ['slack','database'] : ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Reply for your support ticket on CEDS Admin')
                    ->greeting('Support Ticket Update')
                    ->line("Our team has responded to your support ticket you've sent. Please visit the support ticket page for more details")
                    ->action('View Ticket', route('ticket',['ticket'=>$this->response->ticket]))
                    ->line('Thank you for the cooperation!')
                    ->salutation('Regards,');
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $data = [
            $this->response->ticket->token,
            route('ticket',['ticket'=>$this->response->ticket]),
            trim_content('Re: '.$this->response->ticket->title,30),
            $this->response->author->name,
            $this->response->created_at->format('d/m/Y H:i')
        ];
        return (new SlackMessage)
                ->from('CEDS Admin App')
                ->image('http://m.butternutwebsite.com/favicons/apple-touch-icon-180x180.png')
                ->success()
                ->content('New Reply in support ticket!')
                ->attachment(function ($attachment) use ($data) {
                    $attachment->title('ticket token : '.$data[0], $data[1])
                                ->content($data[2])
                                ->fields([ 'Sender'=>$data[3], 'Submitted at' => $data[4] ]);
                });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'ticket_id' => $this->response->ticket->id,
            'message' => trim_content('Re: '.$this->response->ticket->title,40)
        ];
    }
}
