<?php

namespace App\Notifications;

use App\Models\Tickets;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class SupportNote extends Notification
{
    use Queueable;

    protected $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Tickets $ticket)
    {
        $this->ticket = $ticket; 
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack','database'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $data = [
            $this->ticket->token,
            route('ticket',['ticket'=>$this->ticket]),
            trim_content($this->ticket->title,50,'...'),
            $this->ticket->author->name,
            $this->ticket->created_at->format('d/m/Y H:i'),
            $this->ticket->getPriorityReading()
        ];
        return (new SlackMessage)
                ->from('CEDS Admin App')
                ->image('http://m.butternutwebsite.com/favicons/apple-touch-icon-180x180.png')
                ->success()
                ->content('New Support Ticket Submitted!')
                ->attachment(function ($attachment) use ($data) {
                    $attachment->title('ticket token : '.$data[0], $data[1])
                                ->content($data[2])
                                ->fields([ 'Sender'=>$data[3], 'Submitted at' => $data[4], 'priority' => $data[5] ]);
                });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'ticket_id' => $this->ticket->id,
            'message' => trim_content($this->ticket->title,40)
        ];
    }
}
