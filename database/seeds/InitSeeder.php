<?php

use Illuminate\Database\Seeder;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'option_key' => 'feeds_url',
            'option_value' => 'http://ceds.fe.unpad.ac.id',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('settings')->insert([
            'option_key' => 'feeds_number',
            'option_value' => '8',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'name' => "Super Kandjeng",
            'short' => "SuperKandjeng",
            'email' => "nashir@butternutwebsite.com",
            'password' => bcrypt('Sukun313%'),
            'is_admin' => "1",
            'slack_webhook' => "https://hooks.slack.com/services/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",
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'name' => "Led Zeppelin",
            'short' => "LedZep",
            'email' => "ledzep@example.com",
            'password' => bcrypt('blackdog'),
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('category_datas')->insert([
            'parent_id' => '0',
            'title'=> 'UNCATEGORIZED',
            'description'=> 'Uncategorized category and data',
            'created_by'=> '1',
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
