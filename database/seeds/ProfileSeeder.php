<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 * 
	 * @return void
	 */
	public function run()
	{
		$fkey = DB::table('profile_details')->insertGetId([
            'parent_id' => '0',
            'type' => 'richtext',
            'title' => 'CEDS Universitas Padjadjaran',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('c_e_d_s_profiles')->insert([
            'detail_id' => $fkey,
            'title' => "Brief Description",
            'short' => "description",
            'icon' => "fa fa-bullhorn",
            'created_at' => \Carbon\Carbon::now()
        ]);
        $fkey = DB::table('profile_details')->insertGetId([
            'parent_id' => '0',
            'type' => 'repeater',
            'title' => 'Our Service',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'richtext',
            'title' => 'Service #1',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'richtext',
            'title' => 'Service #2',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'richtext',
            'title' => 'Service #3',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('c_e_d_s_profiles')->insert([
            'detail_id' => $fkey,
            'title' => "CEDS Services",
            'short' => "service",
            'icon' => "fa fa-suitcase",
            'created_at' => \Carbon\Carbon::now()
        ]);
        $fkey = DB::table('profile_details')->insertGetId([
            'parent_id' => '0',
            'type' => 'repeater',
            'title' => 'Our Member',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('c_e_d_s_profiles')->insert([
            'detail_id' => $fkey,
            'title' => "CEDS Member",
            'short' => "member",
            'icon' => "fa fa-user",
            'created_at' => \Carbon\Carbon::now()
        ]);
        $parent=DB::table('profile_details')->insertGetId([
            'parent_id' => $fkey,
            'type' => 'repeater',
            'title' => 'name',
            'content' => 'Member #1',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'role',
            'content' => 'Chairman',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'image',
            'title' => 'picture',
            'content' => 'members/images/img.jpg',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'bio',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'email',
            'content' => 'emailmember@unpad.ac.id',
            'created_at' => \Carbon\Carbon::now()
        ]);
        $parent=DB::table('profile_details')->insertGetId([
            'parent_id' => $fkey,
            'type' => 'repeater',
            'title' => 'name',
            'content' => 'Member #2',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'role',
            'content' => 'Secretary',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'image',
            'title' => 'picture',
            'content' => 'members/images/img.jpg',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'bio',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'email',
            'content' => 'emailmember@unpad.ac.id',
            'created_at' => \Carbon\Carbon::now()
        ]);
        $parent=DB::table('profile_details')->insertGetId([
            'parent_id' => $fkey,
            'type' => 'repeater',
            'title' => 'name',
            'content' => 'Member #3',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'role',
            'content' => 'Treasury',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'image',
            'title' => 'picture',
            'content' => 'members/images/img.jpg',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'bio',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $parent,
            'type' => 'text',
            'title' => 'email',
            'content' => 'emailmember@unpad.ac.id',
            'created_at' => \Carbon\Carbon::now()
        ]);
        $fkey = DB::table('profile_details')->insertGetId([
            'parent_id' => '0',
            'type' => 'repeater',
            'title' => 'Contact Us',
            'content' => 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'text',
            'title' => 'Address',
            'content' => 'Jl Cimandiri Bandung Indonesia.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'text',
            'title' => 'Phone',
            'content' => '022 5689 2541',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'text',
            'title' => 'Email',
            'content' => 'ceds@unpad.ac.id',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('profile_details')->insert([
            'parent_id' => $fkey,
            'type' => 'text',
            'title' => 'facebook',
            'content' => 'Jl Cimandiri Bandung Indonesia.',
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('c_e_d_s_profiles')->insert([
            'detail_id' => $fkey,
            'title' => "CEDS Contacts",
            'short' => "contact",
            'icon' => "fa fa-envelope",
            'created_at' => \Carbon\Carbon::now()
        ]);
	}
}