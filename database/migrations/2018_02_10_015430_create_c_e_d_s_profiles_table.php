<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCEDSProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_e_d_s_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('short');
            $table->string('icon');
            $table->timestamps();
            $table->index('detail_id');
            $table->foreign('detail_id')->references('id')->on('profile_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('c_e_d_s_profiles')) {
            Schema::table('c_e_d_s_profiles', function (Blueprint $table){
                $table->dropForeign('c_e_d_s_profiles_detail_id_foreign');
                $table->dropIndex('c_e_d_s_profiles_detail_id_index');
                $table->dropColumn('detail_id');
            });
            Schema::drop('c_e_d_s_profiles');
        }
    }
}
