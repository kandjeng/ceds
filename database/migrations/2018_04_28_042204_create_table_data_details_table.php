<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDataDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_data_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('table_id')->nullable();
            $table->bigInteger('ymax');
            $table->bigInteger('ymin');
            $table->text('column_title');
            $table->text('column_data');
            $table->timestamps();
            $table->index('title');
            $table->foreign('table_id')->references('id')->on('table_datas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('table_data_details')) {
            Schema::table('table_data_details', function (Blueprint $table){
                $table->dropIndex('table_data_details_title_index');
                $table->dropForeign('table_data_details_table_id_foreign');
                $table->dropColumn('table_id');
            });
            Schema::drop('table_data_details');
        }
    }
}
