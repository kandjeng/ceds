<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier',32);
            $table->string('patron');
            $table->string('filename');
            $table->integer('journal_id')->unsigned()->nullable();
            $table->timestamps();
            $table->index('journal_id');
            $table->foreign('journal_id')->references('id')->on('journals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('journal_downloads')) {
            Schema::table('journal_downloads', function (Blueprint $table){
                $table->dropForeign('journal_downloads_journal_id_foreign');
                $table->dropIndex('journal_downloads_journal_id_index');
                $table->dropColumn('journal_id');
            });
            Schema::drop('journal_downloads');
        };
    }
}
