<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDataDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_data_downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier',32);
            $table->string('patron');
            $table->string('filename');
            $table->unsignedInteger('table_id')->nullable();
            $table->timestamps();
            $table->index('table_id');
            $table->foreign('table_id')->references('id')->on('table_datas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('table_data_downloads')) {
            Schema::table('table_data_downloads', function (Blueprint $table){
                $table->dropForeign('table_data_downloads_table_id_foreign');
                $table->dropColumn('table_id');
            });
            Schema::drop('table_data_downloads');
        }
    }
}
