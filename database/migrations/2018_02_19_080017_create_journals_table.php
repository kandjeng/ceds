<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('strand')->unique();
            $table->integer('created_by')->unsigned()->nullable();
            $table->string('title');
            $table->string('author');
            $table->text('short');
            $table->boolean('is_local')->default(1);
            $table->timestamps();
            $table->index('title');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if (Schema::hasTable('journals')) {
            Schema::table('journals', function (Blueprint $table){
                $table->dropIndex('journals_title_index');
                $table->dropForeign('journals_created_by_foreign');
                $table->dropColumn('title');
            });
            Schema::drop('journals');
        };
    }
}
