<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('short');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->timestamps();
            $table->index('title');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('category_datas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('table_datas')) {
            Schema::table('table_datas', function (Blueprint $table){
                $table->dropForeign('table_datas_created_by_foreign');
                $table->dropForeign('table_datas_category_id_foreign');
                $table->dropIndex('table_datas_title_index');
                $table->dropColumn('created_by');
                $table->dropColumn('category_id');
                $table->dropColumn('title');
            });
            Schema::drop('table_datas');
        }
    }
}
