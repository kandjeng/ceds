<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by')->unsigned()->nullable();
            $table->string('token')->unique();
            $table->string('title');
            $table->string('status')->default('open');
            $table->integer('priority');
            $table->text('message');
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('tickets')) {
            Schema::table('tickets', function (Blueprint $table){
                $table->dropForeign('tickets_created_by_foreign');
                $table->dropColumn('created_by');
            });
            Schema::drop('tickets');
        }
    }
}
