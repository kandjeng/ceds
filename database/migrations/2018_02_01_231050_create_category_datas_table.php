<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('title',255);
            $table->text('description');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamps();
            $table->index('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('category_datas')) {
            Schema::table('category_datas', function (Blueprint $table){
                $table->dropForeign('category_datas_created_by_foreign');
                $table->dropIndex('category_datas_created_by_index');
                $table->dropColumn('created_by');
            });
            Schema::drop('category_datas');
        }
    }
}
