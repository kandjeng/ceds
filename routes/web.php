<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
Route::get('/cpadmin/logout', 'HomeController@SignOut');
Route::get('/cpadmin/changepass', 'HomeController@showchangepass');
Route::post('/cpadmin/changepass', 'HomeController@changepass')->name('changePass');
Route::get('/cpadmin/category/{category}','CategoryDataController@showcategory')->name('category');
Route::get('/cpadmin/category/{category}/add','CategoryDataController@showaddcategory')->name('addCategoryShow');
Route::post('/cpadmin/addcategory',"CategoryDataController@addcategory")->name('addCategory');
Route::get('/cpadmin/category/{category}/edit','CategoryDataController@showeditcategory')->name('categoryEdit');
Route::post('/cpadmin/editcategory',"CategoryDataController@editcategory")->name('editCategory');
Route::post('/cpadmin/category/delete','CategoryDataController@deletecategory')->name('categoryDelete');

Route::prefix('/cpadmin/settings')->group(function(){
	Route::get('rssfeed','SettingsController@showRSSsettings')->name('rssSetting');
	Route::post('rssfeed','SettingsController@updateRSS')->name('updateRSS');
});
Route::prefix('/cpadmin/cedsprofile')->group(function(){
	Route::get('detail/{id}','CEDSProfileController@getdetail')->name('detail');
	Route::get('child/{id}','CEDSProfileController@getchilddetail')->name('getDetail');
	Route::get('brief','CEDSProfileController@showeditbrief')->name('briefProfile');
	Route::get('service','CEDSProfileController@showeditservice')->name('serviceProfile');
	Route::get('member','CEDSProfileController@showeditmember')->name('memberProfile');
	Route::get('members','CEDSProfileController@getmembers')->name('getMembers');
	Route::get('member/{id}','CEDSProfileController@getmemberdetail')->name('getMember');
	Route::get('contact','CEDSProfileController@showeditcontact')->name('contactProfile');
	Route::post('edit','CEDSProfileController@editdetail')->name('editProfileDetail');
	Route::post('editmember','CEDSProfileController@editmember')->name('editmember');
	Route::post('addmember','CEDSProfileController@addmember')->name('addmember');
	Route::post('deletemember','CEDSProfileController@deletemember')->name('deleteMember');
	Route::post('editx','CEDSProfileController@editdetailx')->name('editProfileDetailX');
	Route::post('deletex','CEDSProfileController@deletedetailx')->name('deleteProfileDetailX');
});
Route::prefix('/cpadmin/support')->group(function(){
	Route::get('/','SupportController@all');
	Route::get('/{status}','SupportController@all')->name('ticketClosed');
	Route::get('ticket/{ticket}','SupportController@showticket')->name('ticket');
	Route::get('deleteticket/{ticket}','SupportController@deleteticket')->name('deleteTicket');
	Route::post('addticket','SupportController@addticket')->name('addTicket');
	Route::post('editticket','SupportController@editticket')->name('editTicket');
	Route::post('addresponse','SupportController@addresponse')->name('addResponse');
});
Route::prefix('/cpadmin/journal')->group(function(){
	Route::get('/','JournalController@all')->name('indexJournal');
	Route::get('/{id}','JournalController@showjournal')->name('journal');
	Route::post('editjournal','JournalController@edit')->name('editJournal');
	Route::post('addjournal','JournalController@add')->name('addJournal');
	Route::post('deletejournal','JournalController@delete')->name('deleteJournal');
});
Route::prefix('/cpadmin/data')->group(function(){
	Route::get('/{id}','TableDataController@showdata')->name('tabledata');
	Route::get('tags/all.json','TableDataController@getalltags')->name('dataTags');
	Route::post('adddata','TableDataController@add')->name('addData');
	Route::post('editdata','TableDataController@edit')->name('editData');
	Route::post('deletedata','TableDataController@delete')->name('deleteData');
});
Route::get('/cpadmin', 'HomeController@index')->name('home');
Route::get('/downloadxls','StruggleController@getxls');

