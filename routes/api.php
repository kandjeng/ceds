<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', function () {
    return response([1,2,3,4],200);
});
Route::get('maincat','StruggleController@indexmaincategory');
Route::get('showcat/{id}','StruggleController@showcategory');
Route::get('showdata/{id}','StruggleController@showdata');
Route::get('getdetail','StruggleController@getdetail');
Route::get('showsetdata','StruggleController@showsetdata');
Route::get('journals','StruggleController@indexjournal');
Route::get('journal/{id}','StruggleController@showjournal');
Route::post('genjournal','StruggleController@genjournal');
Route::get('feeds','StruggleController@indexfeeds');
Route::get('fetch','StruggleController@fetcharticle');
Route::get('tabrelated','StruggleController@showrelated');
Route::get('cedsprofile', 'StruggleController@cedsprofile');
Route::get('cedscontact', 'StruggleController@cedscontact');
Route::post('preparefile','StruggleController@getsingle');
Route::get('fetch/{identifier}','StruggleController@fetcher');
Route::post('search','StruggleController@search');
Route::post('homescreen','StruggleController@homereq');