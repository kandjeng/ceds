<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ Gravatar::src(Auth::user()->email) }}" alt="Avatar of {{ Auth::user()->name }}">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;">Help</a></li>
                        <li><a href="{{ url('cpadmin/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>
                
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        @if ($unreadNum>0)
                        <span class="badge bg-green">{{$unreadNum}}</span>
                        @endif
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        @foreach ($notifs as $notif)
                        <li>
                            <a href="{{ route('ticket',['ticket'=>$notif->data['ticket_id']]) }}">
                                <span>
                                    @if ($notif->type=='App\Notifications\SupportNote')
                                    <span class="msg-title">New Ticket Submitted</span>
                                    @else
                                    <span class="msg-title">New Reply on a Ticket</span>
                                    @endif
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    {{ $notif->data['message'] }}
                                </span>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <div class="text-center">
                                <a href="{{ url('cpadmin/support') }}">
                                    <strong>Go To Support Center</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->