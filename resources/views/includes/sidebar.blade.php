<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/cpadmin') }}" class="site_title"><i class="fa fa-paw"></i> <span>CEDS econdata!</span></a>
        </div>
        
        <div class="clearfix"></div>
        
        <br />
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a href="{{ url('/cpadmin') }}">
                            <i class="fa fa-bar-chart"></i>
                            Dashboard
                        </a>
                    </li>
                    <li><a><i class="fa fa-sitemap"></i> Data Categories <span class="fa fa-chevron-down"></span></a>
                        {!! render_category_sidebar(get_category_tree()) !!}
                    </li>
                    <li>
                        <a href={{ url('cpadmin/journal') }}><i class="fa fa-file"></i> Journal/Paper</a>
                    </li>
                    <li><a><i class="fa fa-university"></i> CEDS Profile <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('briefProfile') }}"><i class="fa fa-bullhorn"></i> Brief Description</a></li>
                            <li><a href="{{ route('serviceProfile') }}"><i class="fa fa-suitcase"></i> CEDS Services</a></li>
                            <li><a href="{{ route('memberProfile') }}"><i class="fa fa-user"></i> CEDS Member</a></li>
                            <li><a href="{{ route('contactProfile') }}"><i class="fa fa-envelope"></i> CEDS Contacts</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-cogs"></i> Settings <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('rssSetting') }}"><i class="fa fa-rss"></i> Edit RSS Feeds</a></li>
                            <li>
                                <a href="{{ url('cpadmin/support') }}"><i class="fa fa-wechat"></i> Support Center 
                                    @if ($unreadNum>0)
                                        <span class="badge pull-right">{{$unreadNum}}</span>
                                    @endif
                                </a>
                            </li>
                            <li><a href="{{url('/cpadmin/changepass')}}"><i class="fa fa-support"></i> Change Password</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
        
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('cpadmin/logout') }}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>