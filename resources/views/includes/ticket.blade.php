<table class="table table-stripped">
	<thead><tr><th>Token</th><th>Status</th><th>Tickets Title</th><th>Priority</th><th>Date Created</th><th>actions</th></tr></thead>
	<tbody>
		@foreach ($data as $ticket)
		<tr>
			<td>{{ $ticket->token }}</td>
			<td><span class="label label-success">{{ $ticket->status }}</span></td>
			<td>{{ $ticket->title }}</td>
			<td>{{ $ticket->priority }}</td>
			<td>{{ $ticket->created_at }}</td>
			<td>leko</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $data->links() }}