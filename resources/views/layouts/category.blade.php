@extends('blank')

@push('stylesheets')
    <!-- Trumbowyg -->
    <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <style type="text/css">
    	.trumbowyg-box.trumbowyg-editor-visible {min-height: 150px;}.trumbowyg-editor {min-height: 150px;}
    </style>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div id="cat-op" class="pull-right">
    		<a href="{{ route("categoryEdit",$currentcategory) }}" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit Category</a>
    		<button data-name="{!! $currentcategory->title !!}" data-delete="{!!$currentcategory->id!!}" class="btn-delete btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>
    	</div>
    	<h3>Category : {{$title}}</h3>
    	<div class="row">
			<div class="col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Sub-Category</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@if (count($subcategories)>0)
						<table class="table table-striped cattable" id="cat-table">
							<thead>
								<tr>
									<th></th>
									<th></th>
									<th>Data</th>
									<th>Created At</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($subcategories as $subcategory)
									<tr>
										<td><a href="{{ route('category',$subcategory) }}" class="tablink"><i class="glyphicon glyphicon-folder-open"</i></a></td>
										<td><a href="{{ route('category',$subcategory) }}" class="tablink">{{ $subcategory->title }}</a></td>
										<td>{{ $subcategory->ecodata->count() }}</td>
										<td>{{ $subcategory->created_at->diffForHumans() }}</td>
										<td>
											<a href="{{ route('categoryEdit',$subcategory) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
											<button class="btn btn-delete btn-danger btn-xs" data-name="{!! $subcategory->title !!}" data-delete="{!! $subcategory->id !!}"><i class="fa fa-trash-o"></i> Delete</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						@endif
						<a href="{{route('addCategoryShow',$currentcategory)}}" class="btn btn-dark pull-right"><i class="fa fa-plus"></i> Add Sub-category</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Stored Data</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						<div id="econdata" class="data-journal">
							{!! $dataTable->table() !!}
						</div>
						<button id="addData" data-category="{!!$currentcategory->id!!}" class="btn btn-dark pull-right"><i class="fa fa-plus"></i> Add Data to this category</button>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <!-- /page content -->
@endsection
@push('scripts')
	<script src="{{ asset('js/trumbowyg.min.js') }}"></script>
   	<script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
   	<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
	<script type="text/javascript">
		$('#cat-table, #cat-op').on('click','.btn-delete',function(e){
			e.preventDefault(e);
			$('#modals').html(deleteCategory($(this).attr('data-delete'),$(this).attr('data-name')));
			$('#modals').modal('show');
		})
		$('#addData').click(function(){
			$('#modals').html(getModalBig($(this).attr('data-category')));
			$('#modals').modal('show');
			$('#data-tags').tagsinput('refresh');
			$('#data-desc').trumbowyg({
                autogrow: true,
                svgPath: '{{ asset('icons/icons.svg') }}'
            });
		});
		$('#econdata').on('click','.btn-delete',function(e){
			$('#modals').html(deleteData($(this).attr('data-id'),$(this).attr('data-name')));
			$('#modals').modal('show');
		});
		function deleteCategory(id,name){
			return '{!! BootForm::open(['url' => route('categoryDelete'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Delete: '+name+'</h4></div><div class="modal-body"><input type="hidden" name="cat_id" value="'+ id +'"><p class="text-center">This process is irreversible,<br>do you want to delete this category ?<br></p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Cancel</a><button type="submit" class="btn btn-danger">Delete</button></div></div></div>{!! BootForm::close() !!}';
		}
		function deleteData(id,name){
			return '{!! BootForm::open(['url' => route('deleteData'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Delete Data ?</h4></div><div class="modal-body"><input type="hidden" name="data_id" value="'+ id +'"><p class="text-center"><strong>'+name+'</strong><br>do you want to delete this data ?<br></p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Cancel</a><button type="submit" class="btn btn-danger">Delete</button></div></div></div>{!! BootForm::close() !!}';
		}
		function getModalBig(id){
            return '{!! BootForm::open(['id' =>'addData', 'url' => route('addData'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel">{{trim_content($currentcategory->title,50)}} <i class="fa fa-angle-double-right"></i> Add Data</h4></div><div class="modal-body row"><input type="hidden" name="cat_id" value="'+ id +'"><div class="col-md-8 col-xs-12 col-md-offset-2"><div class="form-group"><label class="control-label" for="data-title">Title</label><input id="data-title" type="text" class="form-control" name="title" required></div><div class="form-group"><label class="control-label" for="data-tags">Data table tags</label><input id="data-tags" type="text" class="tags form-control" name="data_tags" required></div><div class="form-group"><label class="control-label" for="data-file">Data File (in xls, xlsx, csv)</label><input id="data-file" type="file" class="form-control" name="datafile" accept=".xls, .xlsx, .csv" required></div><div class="form-group"><label class="control-label" for="data-desc">Data Short Description</label><textarea id="data-desc" class="form-control" name="short" required></textarea></div></div></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Close</a><button type="submit" class="btn btn-primary">Save Data</button></div></div></div>{!! BootForm::close() !!}';
        }
	</script>
	{!! $dataTable->scripts() !!}
@endpush