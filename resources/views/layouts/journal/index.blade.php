@extends('blank')

@push('stylesheets')
    <!-- Trumbowyg -->
    <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
    <!-- DataTables -->
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <style type="text/css">
    	.trumbowyg-box.trumbowyg-editor-visible {min-height: 150px;}.trumbowyg-editor {min-height: 150px;}
    </style>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>CEDS Journal/Paper</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Stored Journal/Paper</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        <div id="journals" class="data-journal">
                            {!! $dataTable->table() !!}
                        </div>
                        <div class="clearfix"></div>
						<div>
                            <button id="addNew" class="btn btn-dark"><i class="fa fa-plus"></i> Add New Journal</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <div id="modalGuide" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Upload Guide</h4>
            </div>
            <div class="modal-body">
              <p>Format file excel (.xls, .xlsx).<br> Untuk data dengan data kuartal dan atau bulanan, unggah dalam format sheet seperti gambar</p>
              <img style="width:100%" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVAAAABUCAYAAADUDm97AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAh7SURBVHhe7Z3LbtRIFIZ5Fd4lYofEvAJSFLHOzBv0SEFiyyxYthBEILEiCyCiF5BskGAR7gkBwv2ubNjW+Niu7qrqsvGtu6vc3y99il0u9+XQ/nLKjZQTJ9dPqXnk+vXr+RbpSx4/fpxvERJ+ZvF5RaCkcRAoiSkIlAQVBEpiCgIlQQWBkpgyM4EeHx+rX79+qZ8/f6ofP36o79+/q2/fvqmvX7+qz58/q0+fPqmPHz+qDx8+qPfv36t3796pt2/fqjdv3qjXr1+rw8ND9erVK3VwcKBevnypXrx4oZ4/f66ePXumnj59qp48eZIKVN4AAEBfoAMljSMfIEJiySw+rwiUNA4CJTEFgZKggkBJTEGgJKggUBJTECgJKgiUxBQESoIKAiUxZcEC3VEbKytqYyffTXK0uaZWkrEx5kEnCLR/QaAkpixUoFqWrkBLnGkFgfYvCJTElMUJ9GhTra1tqs0NW6A7zn5ZEGj/gkBJTFmQQI/U5tqa2jyaFqbsT5bwG8kivzgItH9BoCSmLESgIsk1sWe+XdRxpkv8pEvNZk4HgfYvCJTElPkLdGfD+mKofMk+6VR9QaD9CwIlMWXOAhUhmkv0CbojtSPf0hcv4xFo/4JASUyZfwfqxO5AE2Ea7Wi6hC9uTxFoD4NASUwJT6BmZ1py/1OCQPsXBEpiysIF2iYItH9BoCSmIFASVBAoiSkIlAQVBEpiCgIlQQWBkpgyM4HyN5EAAOpDB0oaRz5AhMSSWXxeEShpHARKYgoCJUEFgZKYgkBJUEGgJKYgUBJUECiJKQiUBBUESmLKzAQqcoOwWMb46gAQMqlAf//+DQEh/zDLGHnfvnoAhAoCDRAEChAHCDRAEChAHCDQAEGg0AbqWJ22tUKgAYJAoQ3UsToItIcgUGgDdawOAu0hCBTaQB2rg0B7SD2BHqlLF04p+Xc8vT35q1SH2+fSsZOXd/ORrrKr/pHHXT+vtvORrrJcF/5IDVZW1XDfd6wdcdaxrB7h1gqBBkg9gSbZO5/Jciw1LdVz6tKXdKDDBCzQ/aFaNf/Q4Ywuuoy2F3WIUpDXlNRtMPIcyxkNOqqr+/7L6hFirTIWKtDd3V3veNfM63m6orZAjS70n71kVwu18+5TEqhAfRd2LtTByBjrjLYXdYhSyF7T6mrR69pXw+RY8fE6uO+/rB4h1ipjYQIVqc1ToDFJtL5A7SX79mVDpuNo8eWM5TqRr2Z83pdNdVrGLmyqS+ljijRNgTriTpMfT845zEeqpu2F773ARKyrQ7XvnefspxKedLAT8ebzRm6HmzPu2EQwetzzPPr89PVMnns0WFGrw/18boL1muvTto6Dwaq3C90fJvIcDu2aJcjrH9fCet3TdUtr6q4U0ueanjupSYi1yliIQLXQ5i3QeT1fW5oIdEqQlsCcYznZPVPfsXzprwU6xhWoLe40efdr3o+tmsYf5tKLaHLx2dvufiK/gfEYqUwHajSel1zUPjm4ItHiEUmM5//hfHkuQ1hTkqhJ+19E0++t6JhI1XxfVg3c9z1VU/M5Ks4NplYZcxeoKbN5CW0Rz9mGZgI1ZJZgdp9TkjM6S7dLtLrXsUDNe6m2QN397Pxm915nI9Bs2ZldqL6L1tw3MY/55v3pscqe1x2TbS0LOU9vN6O9QHMxGqKa7Luv2/e+yiRZdG7VubIdQq0y6EADpKlAi8RoitVC5k11mRmWQL3drBaoKd38mEfMVTIbgRZdiNP7qSSkCxpTdJ5vTPbNc+udL53UeHlriKsJXQi0+rYrsKK5dY6Vzw2jVhncAw2QmQnU86WSfb/UuadZUaB63ukL2fOY3W+ddHPhO1hyLblIpyRcfAFPHy+aU3bMGcuXpiLxtl96dVVH3XXa3ag5p+h9zbIDTQiiVhkLE6gwT4H6xkOla4GOxx1EdEXdaS2BWl9EmeP10ubDnHWP5gWYMPUtvCz5JvfMrHMcgdqP517c/jH7/p9JlfOT/WQ5KrRZkgrd/SKSfbOTnp6TvmejbnYNfI+n98uOVZi78FplLFSg4KdzgUo8Es06RS3EBPcb/MoCNUTs6XKrpu2HOZWgtYS2L253jv2NciZX/zH3As7IJJvMt4QxeYziztc/VizgenQn0EkXWjYnfd1T79k3196361c2t+A5F1qrDAQaII0FurD4/jtT/bQWqIfxxe2KNEDktbZdkgqzqGNohFIrBBogMQlUd6wpvs63Rpbhwi8kvd3Qfkkq9L6OAdUKgQZInAJtfu9TZ1kFmnXJ9hK1DX2uY2i1QqABEpNAu8yyCrRrqGN1EGgPQaDQBupYHQTaQ7Il8XLiqwfUA4FWB4H2EFcqy4SvHgChgkABABqCQAEAGoJAAQAakgp0NBoBAEBNUoEeHx8DAEBNECgAQEMQKABAQxAoAEBDECgAQEMQKABAQyoJ9K8zZyxu3brlndcEebxHjx55jwEAhExlgWrJXblyJd135zQFgQJArNQWaNG+8O9gMB77e33dO+7Ol5/yWGfPnk3lLMelw5XzzXMAAEKjUQdqyk22tfhk2+0mZV/O1/sy57+LF9Nt3c3KHBnTopWfeg4AQKjUvgda1E1q9P1RLUeNFqu5be4L0oXKmPw05wAAhEjtDlTkZn6J5ApRkONahu6cIoHKtn5s81wAgFCpLVD5aQrOXJJrzGW+uUx357vHpLv1PR4AQIg0+hJJBKcFKeNyXKPniGRl3/yiyJ3vHpPuU/bNDhcAIFQqCXReiJhZvgNALAQjUOlo6T4BICaC6kABAGICgQIANASBAgA0BIECADQEgQIANCQVKAAA1OfEw4cP1b1799SdO3fUzZs31Y0bN9S1a9fU1atXAQCghBMHBwdqb29PPXjwQN2/f1/dvXtX3b59W21tbQEAQCFb6n/2LR/TOui8lQAAAABJRU5ErkJggg==" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

          </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
	<script src="{{ asset('js/trumbowyg.min.js') }}"></script>
   	<script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
	<script type="text/javascript">
		$('#addNew').click(function(){
           $('#modals').html(getModalBig());
           $('#modals').modal('show');
           $('#journal-author').tagsinput('refresh');
           $('#journal-desc').trumbowyg({
                autogrow: true,
                svgPath: '{{ asset('icons/icons.svg') }}'
            });
        });
        $('#journals').on('click','.btn-delete',function(){
            $('#modals').html(getModal($(this).attr('data-id')));
            $('#modals').modal('show');
        });
        $('#modals').on('change','#journal-loc',function(){
            if (parseInt($(this).val())) {
                $('#journal-file').prop('required',true);
                $('#j-file').show(400);
                $('#journal-url').removeAttr('required');
                $('#j-url').hide();
            }
            else {
                $('#journal-url').prop('required',true);
                $('#j-url').show(400);
                $('#journal-file').removeAttr('required');
                $('#j-file').hide();
            };
        });
        $('#modals').on('click','#guideUpload',function(){
            console.log('guide');
            $('#modalGuide').modal('show');
        })
        function getModal(id){
            return '{!! BootForm::open(['url' => route('deleteJournal'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Confirm Delete Journal</h4></div><div class="modal-body"><input type="hidden" name="journal-id" value="'+id+'"><p class="text-center">This process is irreversible,<br>do you want to delete this journal?<br></p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Cancel</a><button class="btn btn-danger">Delete</button></div></div></div>{!! BootForm::close() !!}'
        };
        function getModalBig(){
            return '{!! BootForm::open(['id' =>'addJournal', 'url' => route('addJournal'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel">Add New Journal</h4></div><div class="modal-body row"><div class="col-md-8 col-xs-12 col-md-offset-2"><div class="form-group"><label class="control-label" for="journal-title">Title</label><input id="journal-title" type="text" class="form-control" name="title" required></div><div class="form-group"><label class="control-label" for="journal-author">Author (separate by commas)</label><input id="journal-author" type="text" class="form-control" name="author" data-role="tagsinput" placeholder="Add an author" required></div><div class="form-group"><label class="control-label" for="journal-loc">Journal Location</label><select name="is_local" id="journal-loc" class="form-control"><option value="1">On this server</option><option value="0">Remote / other place via URL</option></select></div><div id="j-file" class="form-group"><label class="control-label" for="journal-file">Journal/Paper File (in PDF) </label><input id="journal-file" type="file" class="form-control" name="textfile" accept=".pdf" required><a href="#" style="color:#f99" id="guideUpload">[..read the guide..]</a></div><div id="j-url" class="form-group" style="display:none"><label class="control-label" for="journal-url">Journal/Paper URL link</label><input id="journal-url" type="text" class="form-control" name="journal_url"></div><div class="form-group"><label class="control-label" for="journal-desc">Journal Short Description</label><textarea id="journal-desc" class="form-control" name="short" required></textarea></div></div></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Close</a><button type="submit" class="btn btn-primary">Save Journal</button></div></div></div>{!! BootForm::close() !!}';
        }
	</script>
    {!! $dataTable->scripts() !!}
@endpush