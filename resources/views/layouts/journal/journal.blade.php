@extends('blank')

@push('stylesheets')
    <!-- Trumbowyg -->
    <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div id="cat-op" class="pull-right">
    		<a href="{{ url('cpadmin/journal') }}" class="btn btn-default" data-dismiss="modal"><i class="fa fa-angle-double-left"></i> Back to journals</a>
    	</div>
    	<h3>CEDS Journal</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit journal</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'editJournal', 'url' => route('editJournal'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}
							<div class="form-group">
								<input type="hidden" name="journal-id" value="{!!$journal->id!!}">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="journal-title">Journal Title</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="journal-title" type="text" class="form-control" name="title" value="{{$journal->title}}" required>
	    						</div>
	    					</div>
							<div class="form-group">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="journal-author">Author (separate by commas)</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="journal-author" type="text" data-role="tagsinput" class="form-control" name="author" value="{{ implode(',',json_decode($journal->author)) }}" placeholder="Add an author" required>
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="journal-loc">Journal Location</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<select name="is_local" id="journal-loc" class="form-control">
	    								<option value="1" {{$journal->is_local?'selected':''}}>On this server</option>
	    								<option value="0" {{$journal->is_local?'':'selected'}}>Remote / other place via URL</option>
	    							</select>
	    						</div>
	    					</div>
	    					<div id="j-file" class="form-group" {!!$journal->is_local?'':'style="display:none"'!!}>
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="journal-file">Replace the PDF File ?</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="journal-file" type="file" class="form-control" name="textfile" accept=".pdf">
	    						</div>
	    					</div>
	    					<div id="j-url" class="form-group" {!!$journal->is_local?'style="display:none"':''!!}>
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="journal-url">Replace Journal/Paper URL link ?</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="journal-url" type="text" class="form-control" name="journal_url" {!!$journal->is_local?'':'value="'.$journal->strand.'" required'!!}>
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="journal-desc">Journal's Short Description</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<textarea id="journal-desc" class="form-control" name="short" required>{!! $journal->short !!}</textarea>
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
		    						<button type="submit" class="btn btn-primary">Save Journal</button>
		    						<button type="button" data-toggle="modal" data-target="#modals" class="btn btn-danger btn-delete">delete journal</button>
    							</div>
	    					</div>		    				
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	{!! BootForm::open(['url' => route('deleteJournal'), 'method' => 'post']) !!}
    		<div class="modal-dialog modal-sm">
    			<div class="modal-content">
	    			<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    					<span aria-hidden="true">×</span>
	    				</button>
	    				<h4 class="modal-title">Confirm Delete Journal</h4>
	    			</div>
	    			<div class="modal-body">
	    				<input type="hidden" name="journal-id" value="{{$journal->id}}">
	    				<p class="text-center">This process is irreversible,<br>do you want to delete this journal?<br></p>
	    			</div>
	    			<div class="modal-footer">
	    				<a class="btn btn-default" data-dismiss="modal">Cancel</a>
	    				<button class="btn btn-danger">Delete</button>
	    			</div>
	    		</div>
	    	</div>
	    {!! BootForm::close() !!}
    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
	<script src="{{ asset('js/trumbowyg.min.js') }}"></script>
   	<script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#journal-desc').trumbowyg({
				autogrow: true,
				svgPath: '{{ asset('icons/icons.svg') }}'			
			});
		});
		$('#journal-loc').change(function(){
			var local = {{$journal->is_local}};
			if (parseInt($(this).val())) {
                $('#j-file').show(400);
                $('#journal-url').removeAttr('required');
                $('#j-url').hide();
                if (!local){
                	$('#journal-file').prop('required',true);
                }
            }
            else {
                $('#journal-url').prop('required',true);
                $('#j-url').show(400);
                $('#journal-file').removeAttr('required');
                $('#j-file').hide();
            };
        });
	</script>
@endpush