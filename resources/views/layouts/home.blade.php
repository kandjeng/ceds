@extends('blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
		<div class="row top_tiles">
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-database"></i></div>
					<div class="count">{{$stats['data_total']}}</div>
					<h3>Data Stored</h3>
					<p>The total economic data stored.</p>
				</div>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-download"></i></div>
					<div class="count">{{$stats['data_total_dl']}}</div>
					<h3>Data Downloaded</h3>
					<p>The total download from all datas</p>
				</div>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-newspaper-o"></i></div>
					<div class="count">{{$stats['journal_total']}}</div>
					<h3>Journal Stored</h3>
					<p>The total Journal Stored.</p>
				</div>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-cloud-download"></i></div>
					<div class="count">{{$stats['journal_total_dl']}}</div>
					<h3>Journal Downloaded</h3>
					<p>The total download from all journals.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="x_panel tile">
					<div class="x_title">
						<h2>Download Recently</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<div id="chart">
									<canvas id="dl_graph" width="794" height="450"></canvas>
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12 bg-white">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="popTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation" class="active">
										<a href="#data_recent_downloaded" role="tab" id="data-recentdl-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-database"></i> EcoData</a>
									</li>
									<li role="presentation" class="">
										<a href="#journal_recent_downloaded" role="tab" id="journal-recentdl-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-newspaper-o"></i> Journal</a>
									</li>
								</ul>
								<div id="popContent" class="tab-content">
										<div role="tabpanel" class="tab-pane fade active in" id="data_recent_downloaded" aria-labelledby="data-recentdl-tab">
											<table class="table table-striped">
												<tbody>
													@foreach ($stats['data_recent_downloaded'] as $key=>$data)
													<tr>
														<td><i class="fa fa-newspaper-o"></i> {{trim_content($data->title,30)}}</td>
														<td>{{$data->created_at->diffForHumans()}}</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										<div role="tabpanel" class="tab-pane" id="journal_recent_downloaded" aria-labelledby="journal-recentdl-tab">
											<table class="table table-striped">
												<tbody>
													@foreach ($stats['journal_recent_download'] as $key=>$data)
													<tr>
														<td><i class="fa fa-newspaper-o"></i> {{trim_content($data->title,30)}}</td>
														<td>{{$data->created_at->diffForHumans()}}</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
								</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><i class="fa fa-rss-square"></i> RSS Feeds</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#" role="button"><i class="fa fa-wrench"></i></a></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
                    <ul class="list-unstyled timeline widget">
                      @foreach ($items as $key => $item)
                      @if ($key>=$feednum)
                      	@break
                      @endif
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title"><a href="{{ $item->get_permalink() }}">{{$item->get_title()}}</a></h2>
                            <div class="byline">
                              <a href="{{$permalink}}"><img src="https://www.google.com/s2/favicons?domain={{$site}}"></a>
                              <span>&emsp;{{ $item->get_date() }}</span>
                            </div>
                          </div>
                        </div>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
            	<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel tile">
						<div class="x_title">
							<h2>Data Log</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="DataTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation" class="active">
										<a href="#data_popular_content" id="data-popular-tab" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-bullhorn"></i> Popular Data</a>
									</li>
									<li role="presentation" class="">
										<a href="#data_recent_content" role="tab" id="data-recent-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-bolt"></i> Recent Data Uploaded</a>
									</li>
								</ul>
								<div id="myTabContent" class="tab-content">
									<div role="tabpanel" class="tab-pane fade active in" id="data_popular_content" aria-labelledby="datas-latest-dl">
										<table class="table dashtable">
											<thead>
												<tr>
													<th></th>
													<th>Data Title</th>
													<th>Downloaded</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach ($stats['data_popular'] as $key=>$data)
												<tr>
													<td scope="row">{{$key+1}}</td>
													<td>{{trim_content($data->title,50)}}</td>
													<td>{{$data->download_count}}</td>
													<td><a href="{{ route('tabledata',['id'=>$data->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a></td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
									<div role="tabpanel" class="tab-pane fade" id="data_recent_content" aria-labelledby="datas-latest-dl">
										<table class="table dashtable">
											<thead>
												<tr>
													<th></th>
													<th>Data Title</th>
													<th>Uploaded</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach ($stats['data_latest'] as $key=>$data)
												<tr>
													<td scope="row">{{$key+1}}</td>
													<td>{{trim_content($data->title,50)}}</td>
													<td>{{$data->created_at->diffForHumans()}}</td>
													<td><a href="{{ route('tabledata',['id'=>$data->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a></td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel tile">
						<div class="x_title">
							<h2>Journal Log</h2>
							<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="JournalTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation" class="active">
										<a href="#journal_popular_content" id="journal-popular-tab" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-bullhorn"></i> Popular Journal</a>
									</li>
									<li role="presentation" class="">
										<a href="#journal_recent_content" role="tab" id="journal-recent-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-bolt"></i> Recent Journals</a>
									</li>
								</ul>
								<div id="myTabContent" class="tab-content">
									<div role="tabpanel" class="tab-pane fade active in" id="journal_popular_content" aria-labelledby="journals-popular-tab">
										<table class="table dashtable">
											<thead>
												<tr>
													<th></th>
													<th>Paper Title</th>
													<th>Downloaded</th>
													<th></th>
												</tr>
												</thead>
											<tbody>
												@foreach ($stats['journal_popular'] as $key=>$paper)
												<tr>
													<td scope="row">{{$key+1}}</td>
													<td>{{trim_content($paper->title,50)}}</td>
													<td>{{$paper->download_count}}</td>
													<td><a href="{{ route('journal',['id'=>$paper->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a></td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
									<div role="tabpanel" class="tab-pane fade" id="journal_recent_content" aria-labelledby="journal-recent-tab">
										<table class="table dashtable">
											<thead>
												<tr>
													<th></th>
													<th>Paper Title</th>
													<th>Uploaded</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach ($stats['journal_latest'] as $key=>$paper)
												<tr>
													<td scope="row">{{$key+1}}</td>
													<td>{{trim_content($paper->title,50)}}</td>
													<td>{{$paper->created_at->diffForHumans()}}</td>
													<td><a href="{{ route('journal',['id'=>$paper->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a></td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
		</div>
    </div>
    <!-- /page content -->
@endsection
@push ('scripts')
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.0/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.0/Chart.min.js"></script>
    <script>
        $(document).ready(function(){
            var ctx = document.getElementById("dl_graph").getContext("2d");

            var data = {
              labels: [
								@foreach($recent_data as $key => $item)
								"{{$key}}",
								@endforeach
              ],
              datasets: [{
                label: "Ecodata",
                backgroundColor: "#2a3f54",
                yAxisID:'data',
                data: [
                  @foreach($recent_data as $key => $item)
                  "{{$item['data']}}",
                  @endforeach
                ]
              }, {
                label: "Journal",
                backgroundColor: "#6d95bd",
                yAxisID:'journal',
                data: [
									@foreach($recent_data as $key => $item)
                  "{{$item['journal']}}",
                  @endforeach
                ]
              }]
            };
            
            var myBarChart = new Chart(ctx, {
              type: 'bar',
              data: data,
              options: {
                barValueSpacing: 20,
                scales: {
                  yAxes: [{
										id:'data',
										type:'linear',
										position:'left',
										ticks:{
												fixedStepSize:1
										}
                  },{
                     id:'journal',
                     type: 'linear',
                     position:'right',
                     display:false
									}],
									xAxes: [{
										ticks:{
											fontSize: 11,
											display:false,
										}
									}]
                }
              }
            });
        })
    </script>
@endpush