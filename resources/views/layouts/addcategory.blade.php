@extends('blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Add Category</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Adding New Category/Sub-category</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addcategory', 'url' => route('addCategory'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="parent-id">Parent Category</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select id="parent-id" name="parent-id" class="form-control cold-md-5 col-xs-12">
										@foreach ($category_tree as $category)
											<option value="{{$category['id']}}" class="{{$category['top']}}" {{($category['id']==$current)?'selected':''}}>
												{!!$category['title']!!}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="category-title">Category Title <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="category-title" type="text" class="form-control col-md-5 col-xs-12" name="title" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="category-description">Category Description</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea id="category-description" type="text" class="form-control col-md-5 col-xs-12" name="description" rows="6"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
									@if ($current)
									<a href="{{ route('category',$current) }}" class="btn btn-default">return to previous page</a>
									@else
									<a href="{{ route('home') }}" class="btn btn-default">back to home</a>
									@endif
									<button type="submit" class="btn btn-success">Add Category</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- /page content -->
@endsection