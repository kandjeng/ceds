@extends('blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div id="cat-op" class="pull-right">
    		<a href="{{ route('category',$current) }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> return to category</a>
		</div>
    	<h3>Edit Category</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit Category/Sub-category</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addcategory', 'url' => route('editCategory'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
							<input type="hidden" name="current_id" value="{{$current['id']}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="parent-id">Parent Category</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select id="parent-id" name="parent-id" class="form-control cold-md-5 col-xs-12">
										@foreach ($category_tree as $category)
											<option value="{{$category['id']}}" class="{{$category['top']}}" {{($category['id']==$current['parent_id'])?'selected':''}}>
												{!!$category['title']!!}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="category-title">Category Title <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="category-title" type="text" class="form-control col-md-5 col-xs-12" name="title" value="{{$current['title']}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="category-description">Category Description</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea id="category-description" type="text" class="form-control col-md-5 col-xs-12" name="description" rows="6">{{$current['description']}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
									<button type="submit" class="btn btn-success">save</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- /page content -->
@endsection