@extends('blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>RSS Feeds</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>edit RSS Settings</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'RSSsetting', 'url' => route('updateRSS'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="feeds-url">Site URL <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="feeds-url" type="text" class="form-control col-md-5 col-xs-12" name="feeds-url" value="{{$feeds_url}}" required>
									<span>which website to fetch the feeds from</span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="feeds-number">Number of Feeds <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="feeds-number" type="number" class="form-control col-md-5 col-xs-12" name="feeds-number" value="{{$feeds_number}}" required>
									<span>insert how many feeds will be displayed</span>
								</div>
							</div>
							<br>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
									<a href="{{ route('home') }}" class="btn btn-default">back to home</a>
									<button type="submit" class="btn btn-success">Save Settings</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- /page content -->
@endsection