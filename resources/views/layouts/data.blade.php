@extends('blank')

@push('stylesheets')
    <!-- Trumbowyg -->
    <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div id="cat-op" class="pull-right">
    		<a href="{{ route('category',$ecodata->category) }}" class="btn btn-default" data-dismiss="modal"><i class="fa fa-angle-double-left"></i> Back to category</a>
    	</div>
    	<h3>Economic Data</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>{{$ecodata->category->title}} <i class="fa fa-angle-double-right"></i> Edit Data</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'editData', 'url' => route('editData'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}
							<div class="form-group">
								<input type="hidden" name="data_id" value="{!!$ecodata->id!!}">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="data-title">Data Title</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="data-title" type="text" class="form-control" name="title" value="{{$ecodata->title}}" required>
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="data-categ">Data Category</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<select id="data-categ" name="category_id" class="form-control cold-md-5 col-xs-12">
										@foreach ($categories as $category)
											<option value="{{$category['id']}}" class="{{$category['top']}}" {{($category['id']==$ecodata['category_id'])?'selected':''}}>
												{!!$category['title']!!}
											</option>
										@endforeach
									</select>
	    						</div>
	    					</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="data-tags">Data Tags</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="data-tags" type="text" class="form-control" name="data_tags" value="{{$tags}}" data-role="tagsinput" placeholder="Add or select tags" required>
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="data-file">Replace the Data Content ?</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<input id="data-file" type="file" class="form-control" name="datafile" accept=".xls, .xlsx, .csv">
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<h4 class="edit-store col-xs-12 col-md-11">
	    						@foreach($typedata as $type)
	    							<span class="label label-info label-store">{{$type}}</span>
	    						@endforeach
	    						</h4>
	    					</div>
	    					<div class="form-group">
	    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="data-desc">Data Short Description</label>
	    						<div class="col-md-8 col-sm-9 col-xs-12">
	    							<textarea id="data-desc" class="form-control" name="short" required>{!! $ecodata->short !!}</textarea>
	    						</div>
	    					</div>
	    					<div class="form-group">
	    						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
		    						<button type="submit" class="btn btn-primary">Save Data</button>
		    						<button type="button" data-toggle="modal" data-target="#modals" class="btn btn-danger btn-delete">delete data</button>
    							</div>
	    					</div>		    				
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	{!! BootForm::open(['url' => route('deleteData'), 'method' => 'post']) !!}
    		<div class="modal-dialog modal-sm">
    			<div class="modal-content">
	    			<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    					<span aria-hidden="true">×</span>
	    				</button>
	    				<h4 class="modal-title">Delete Data ?</h4>
	    			</div>
	    			<div class="modal-body">
	    				<input type="hidden" name="data_id" value="{{$ecodata->id}}">
	    				<p class="text-center"><strong>{{$ecodata->title}}</strong><br>do you want to delete this data?<br></p>
	    			</div>
	    			<div class="modal-footer">
	    				<a class="btn btn-default" data-dismiss="modal">Cancel</a>
	    				<button class="btn btn-danger">Delete</button>
	    			</div>
	    		</div>
	    	</div>
	    {!! BootForm::close() !!}
    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
	<script src="{{ asset('js/trumbowyg.min.js') }}"></script>
   	<script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
	<script type="text/javascript">
		
			var citynames = new Bloodhound({
			  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
			  queryTokenizer: Bloodhound.tokenizers.whitespace,
			  prefetch : '{{ route('dataTags') }}'
			    
			});
			citynames.initialize();

			$('#data-tags').tagsinput({
			  	typeaheadjs: [{
				      minLength: 1,
				      highlight: true,
				},{
				    minlength: 1,
				    name: 'citynames',
				    displayKey: 'name',
				    valueKey: 'name',
				    source: citynames.ttAdapter()
				}],
				freeInput: true
			});
			
			$('#data-desc').trumbowyg({
				autogrow: true,
				svgPath: '{{ asset('icons/icons.svg') }}'			
			});
		
	</script>
@endpush