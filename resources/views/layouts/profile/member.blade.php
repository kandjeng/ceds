@extends('blank')

@push('stylesheets')
    	<!-- Trumbowyg -->
        <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>CEDS Member</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit CEDS Members Lineup</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'editmember', 'url' => route('editProfileDetail'), 'method' => 'post', 'class' => 'form-horizontal']) !!}
							<input id="id" type="hidden" name="detail-id" value="{{$id}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Member Page Title</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="title" type="text" class="form-control" name="title" value="{{$data['title']}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="content">Member Page Content</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<textarea id="content" class="form-control" name="content" required>{!! $data['content'] !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="content">Members</label>
								<div class="col-md-10 col-sm-9 col-xs-12">
									<div id="membersall">
										<div class="ajax-preload"><i class="fa fa-refresh fa-spin"></i></div>
									</div>
									<a id="addnew" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> new</a>
								</div>
							</div>
							<br>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
									<button type="submit" class="btn btn-success">Save Changes</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <!-- /page content -->
@endsection
@push ('scripts')
	<!-- Trumbowyg -->
    <script src="{{ asset('js/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			$('#content').trumbowyg({
				svgPath: '{{ asset('icons/icons.svg') }}'
			});
			refreshList();
			$('#addnew').click(function(){
				showEdit(0);
			})
		});
		$('#modals').on('change','#upload-pic',function(){
			if (this.files[0].size > 1024000){
				alert("Allowed file size exceeded. (Max. 1 MB)")
				this.value = null;
			}
		});
		$('#modals').on('submit','#memberEdit',function(e){
			e.preventDefault(e);
			var furl = ($('#member-id').val()==0)?'{{ route('addmember') }}':'{{ route('editmember') }}';
			$.ajax({
			    url: furl,
			    data: new FormData(this),
			    type: 'POST',
			    dataType: 'json',
			    contentType: false,
			    processData: false,
			    success: function(response){
			    	new PNotify({
	                    title: response.title,
	                    text: response.text,
	                    type: response.type,
	                    styling : 'bootstrap3'
	                });
	                refreshList();
			    },
			    error: function(response){
			    	console.log(response);
			    }
			});
			$('#modals').modal('hide');
		})
		function showEdit(id){
			$('#modals').html('<div class="ajax-preload modal-preload"><i class="fa fa-refresh fa-spin"></i></div>');
			$('#modals').modal('show'); 
			if (id==0){
				$('#modals').html(getModalBig(0))
			}
			else{
				$.getJSON('/cpadmin/cedsprofile/member/' + id,function(data){
					$('#modals').html(getModalBig(id,data));
				});
			}
		};
		function showDelete(id){
			var modalDOM= '<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Confirm Delete Member</h4></div><div class="modal-body"><p>This process is irreversible, are you sure want to delete this member ?<br></p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Cancel</a><button class="btn btn-danger" onclick="confirmDelete('+id+')">Delete</button></div></div></div>';
			$('#modals').html(modalDOM);
			$('#modals').modal('show');
		}
		function confirmDelete(id){
			var data = {detailid: id};
			$.post('{{ route('deleteMember') }}', data, function(response){
					new PNotify({
	                    title: response.title,
	                    text: response.text,
	                    type: response.type,
	                    styling : 'bootstrap3'
	                });
	                refreshList();
			},'json');
			$('#modals').modal('hide');
		}
		function refreshList(){
			$('#membersall').html('<div class="ajax-preload"><i class="fa fa-refresh fa-spin"></i></div>');
			$.getJSON('{{ route('getMembers') }}', function(data){ 
				var content='';
				$.each(data,function(index,element){
					content += '<div class="col-sm-6 col-xs-12 profile_details"><div class="well profile_view"><div class="col-sm-12"><h4 class="brief"><i>'+element.content+'</i></h4><div class="left col-xs-7"><h2>'+element.role+'</h2><hr><p>'+element.bio+'</p></div><div class="right col-xs-5 text-center"><img src="/'+element.picture+'" alt="" class="img-circle img-responsive"></div></div><div class="col-xs-12 bottom"><div class="col-xs-12 emphasis"><p><i class="fa fa-envelope"></i> '+element.email+'</p></div><div class="col-xs-12 col-sm-6 col-sm-offset-6 emphasis text-right"><button type="button" class="btn btn-mbr btn-primary btn-xs" onclick="showEdit('+element.id+')"><i class="fa fa-edit"></i> edit</button><button type="button" class="btn btn-mbr btn-danger btn-xs" onclick="showDelete('+element.id+')"><i class="fa fa-trash"> </i> delete</button></div></div></div></div>';
				});
				$('#membersall').html(content);
			});
		};
		function getModalBig(id,data = []){
			return '{!! BootForm::open(['id' =>'memberEdit', 'url' => '', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}<div class="modal-dialog modal-lg"><div class="modal-content"><input id="member-id" type="hidden" name="detail-id" value="'+id+'"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel">'+((id==0)?'Add New Member':'Edit CEDS Member Info')+'</h4></div><div class="modal-body row"><div class="col-md-8 col-xs-12 col-md-offset-2"><form class="form-horizontal"><div class="form-group"><label class="control-label col-md-3 col-xs-12" for="edit-member-name">Member Name</label><div class="col-md-9 col-xs-12"><input id="edit-member-name" type="text" class="form-control" name="name" value="'+((id==0)?'':data['content'])+'" required></div></div><div class="form-group"><label class="control-label col-md-3 col-xs-12" for="edit-member-role">Member Role</label><div class="col-md-9 col-xs-12"><input id="edit-member-role" type="text" class="form-control" name="role" value="'+((id==0)?'':data['role'])+'" required></div></div><div class="form-group"><label class="control-label col-md-3 col-xs-12" for="edit-member-bio">Short Bio</label><div class="col-md-9 col-xs-12"><textarea id="edit-member-bio" class="form-control" name="bio" rows="5" required>'+((id==0)?'':data['bio'])+'</textarea></div></div><div class="form-group"><label class="control-label col-md-3 col-xs-12" for="edit-email-address">Email Address</label><div class="col-md-9 col-xs-12"><input id="edit-email-address" type="email" class="form-control" name="email" value="'+((id==0)?'':data['email'])+'" required></div></div><div class="form-group"><label class="control-label col-md-3 col-xs-12">Profile Pic</label><div class="col-md-9 col-xs-12"><div class="col-xs-4 text-center"><img src="/'+((id==0)?'members/images/user.png':data['picture'])+'" class="img-responsive"></div><div class="col-xs-8"><label class="control-label" for="upload-pic">Upload new Picture (max: 1 MB)</label><input id="upload-pic" type="file" name="photo" class="form-control" accept="image/*"></div></div></div></form></div></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Close</a><button type="submit" class="btn btn-primary">Save</button></div></div></div>{!! BootForm::close() !!}';
		}
	</script>
@endpush
