@extends('blank')

@push('stylesheets')
    	<!-- Trumbowyg -->
        <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>CEDS Profile</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit Brief Description</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'editbrief', 'url' => route('editProfileDetail'), 'method' => 'post', 'class' => 'form-horizontal']) !!}
							<input type="hidden" name="detail-id" value="{{$id}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Description Title</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="title" type="text" class="form-control" name="title" value="{{$data['title']}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="content">Description Content</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<textarea id="content" class="form-control" name="content" required>{!! $data['content'] !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
									<button type="submit" class="btn btn-success">Save Changes</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- /page content -->
@endsection
@push ('scripts')
	<!-- Trumbowyg -->
    <script src="{{ asset('js/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script type="text/javascript">
		$('#content').trumbowyg({
			svgPath: '{{ asset('icons/icons.svg') }}'
		});
	</script>
@endpush