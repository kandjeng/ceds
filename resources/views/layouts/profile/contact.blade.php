@extends('blank')

@push('stylesheets')
	    <!-- Trumbowyg -->
        <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main" style="position:relative">
    	<h3>CEDS Contact</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit CEDS Contact lists</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'editbrief', 'url' => route('editProfileDetail'), 'method' => 'post', 'class' => 'form-horizontal']) !!}
							<input id="id" type="hidden" name="detail-id" value="{{$id}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Contact Page Title</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="title" type="text" class="form-control" name="title" value="{{$data['title']}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="content">Contact Page Content</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<textarea id="content" class="form-control" name="content" required>{!! $data['content'] !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12" for="content">List of Contacts</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
										<div class="ajax-preload">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<a id="addnew" class="btn btn-primary pull-right" onclick="editContact(0)"><i class="fa fa-plus"></i> new</a>
								</div>

							</div>
							<br>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
									<button type="submit" class="btn btn-success">Save Changes</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <!-- /page content -->
@endsection
@push ('scripts')
	<!-- Trumbowyg -->
    <script src="{{ asset('js/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			$('#content').trumbowyg({
				svgPath: '{{ asset('icons/icons.svg') }}'
			});
			refreshList();
			$('#addnew').click(function(){
				editContact(0);
			})
		});
		$('#modals').on('submit','#detailForm',function(e){
			e.preventDefault(e);
			var data= $(this).serializeArray();
			data.push({name:'parent',value:{{$id}} });
			$.post('{{ route('editProfileDetailX') }}', data, function(response){
					new PNotify({
	                    title: response.title,
	                    text: response.text,
	                    type: response.type,
	                    styling : 'bootstrap3'
	                });
	                refreshList();
			},'json');
			$('#modals').modal('hide');
		});
		function refreshList(){
			$('#accordion').html('<div class="ajax-preload"><i class="fa fa-refresh fa-spin"></i></div>');
			$.getJSON('{{ route('getDetail',['id'=>$data['id']]) }}', function(data){ 
				var content='';
				$.each(data,function(index,element){
					content += '<div class="panel"><a class="panel-heading collapsed" role="tab" id="service'+index+'" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+'" aria-expanded="false" aria-controls="collapse'+index+'"><h4 class="panel-title">'+element.title+'</h4></a><div id="collapse'+index+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="service'+index+'" aria-expanded="false" style="height: 0px;"><div class="panel-body">'+element.content+'<div class="btn-group-s"><button type="button" class="btn btn-xs btn-dark" onclick="editContact('+element.id+')">edit contact</button><button type="button" class="btn btn-danger btn-xs" onclick="deleteContact('+element.id+')">delete contact</button></div></div></div></div>';
				});
				$('#accordion').html(content);
			});
		};
		function deleteContact(id){
			var modalDOM= '<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Confirm Delete Contact</h4></div><div class="modal-body"><p>This process is irreversible, are you sure want to delete this contact ?<br></p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Cancel</a><button class="btn btn-danger" onclick="confirmDelete('+id+')">Delete</button></div></div></div>';
			$('#modals').html(modalDOM);
			$('#modals').modal('show');
		}
		function confirmDelete(id){
			var data = {detailid: id};
			$.post('{{ route('deleteProfileDetailX') }}', data, function(response){
					new PNotify({
	                    title: response.title,
	                    text: response.text,
	                    type: response.type,
	                    styling : 'bootstrap3'
	                });
	                refreshList();
			},'json');
			$('#modals').modal('hide');
		}
		function editContact(id){
			$('#modals').html('<div class="ajax-preload modal-preload"><i class="fa fa-refresh fa-spin"></i></div>');
			$('#modals').modal('show'); 
			if (id==0){
				var modalDOM = getModalBig();
				$('#modals').html(modalDOM);
			}
			else{
				$.getJSON('/cpadmin/cedsprofile/detail/' + id,function(data){
					var modalDOM = getModalBig(id,data.title,data.content);
					$('#modals').html(modalDOM);
				});
			}
		}
		
		function getModalBig(id=0,title='',content=''){
			return '{!! BootForm::open(['id' =>'detailForm', 'url' => '', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}<div class="modal-dialog modal-lg"><div class="modal-content"><input id="service-id" type="hidden" name="detail-id" value="'+id+'"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel">'+((id==0)?'Add New Contact':'Edit Contact')+'</h4></div><div class="modal-body row"><div class="col-md-8 col-xs-12 col-md-offset-2"><div class="form-group"><label class="control-label" for="edit-title-description">Title</label><input id="edit-title-detail" type="text" class="form-control" name="title" value="'+title+'" required></div><div class="form-group"><label class="control-label" for="edit-content-detail">Content</label><textarea id="edit-content-detail" class="form-control" name="content" required>'+content+'</textarea></div></div></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Close</a><button type="submit" class="btn btn-primary">Save</button></div></div></div>{!! BootForm::close() !!}';
		}
	</script>
@endpush
