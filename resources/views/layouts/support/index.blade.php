@extends('blank')

@push('stylesheets')
    <!-- Trumbowyg -->
    <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Support Center</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Support Tickets</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						<table class="table table-stripped">
							<thead><tr><th>Status</th><th>Token</th><th>Tickets Title</th><th>Submitted</th>{!! ($status=='closed')?'<th>Completed</th>':'<th>Priority</th>' !!}<th></th></tr></thead>
							<tbody {!! $status=='closed'?'class="closed-list"':'' !!}>
								@foreach ($tickets as $ticket)
								<tr{!!$ticket->unread?' class="unread"':''!!}>
									<td><span class="label {{$status=='closed'?'label-default':$state[$ticket->status]}}">{{ $ticket->status }}</span></td>
									<td><a href="{{ route('ticket',['ticket'=>$ticket]) }}">{{ $ticket->token }}</a></td>
									<td><a href="{{ route('ticket',['ticket'=>$ticket]) }}">{{ $ticket->title }}</a></td>
									<td>{{ $ticket->created_at->diffForHumans() }}</td>
									@if ($status=='closed')
									<td>{{ $ticket->updated_at->diffForHumans() }}</td>
									@else
									<td class="priority">
										<h4 class="label {{$priority[$ticket->priority]['class']}}">{{ $priority[$ticket->priority]['label'] }}</h4>
									</td>
									@endif
									<td>
										<a href="{{ route('ticket',['ticket'=>$ticket]) }}" class="btn btn-primary btn-xs btn-mb0">
											<i class="fa fa-eye"></i> view
										</a>
										@if (Auth::user()->is_admin && $status=='closed')
										<a href="{{ route('deleteTicket',['ticket'=>$ticket]) }}" class="btn btn-danger btn-xs btn-mb0">
											<i class="fa fa-trash"></i> delete
										</a>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $tickets->links() }}
						<div class="text-right">
							@if ($status=='closed')
							<a href="{{ url('cpadmin/support') }}" id="filterTicket" class="btn btn-default">Active Tickets</a>
							@else
							<a href="{{route('ticketClosed',['status'=>'closed'])}}" id="filterTicket" class="btn btn-default">View Closed Tickets</a>
							@endif
							<button class="btn btn-primary" data-toggle="modal" data-target="#modals">Create New Ticket</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	{!! BootForm::open(['id' =>'addTicket', 'url' => route('addTicket'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
    	<div class="modal-dialog modal-lg">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
    				<h4 class="modal-title" id="myModalLabel">Create New Ticket</h4>
    			</div>
    			<div class="modal-body row">
    				<div class="col-md-8 col-xs-12 col-md-offset-2">
    					<div class="form-group">
    						<label class="control-label" for="ticket-title">Title</label>
    						<input id="ticket-title" type="text" class="form-control" name="title" required>
    					</div>
    					<div class="form-group">
    						<label class="control-label" for="ticket-priority">Priority</label>
    						<select id="ticket-priority" class="form-control" name="priority" required>
    							<option value="0">Minor Problem</option>
    							<option value="1">Medium Problem</option>
    							<option value="2">Urgent Matter</option>
    						</select>
    					</div>
    					<div class="form-group">
    						<label class="control-label" for="ticket-message">Message</label>
    						<textarea id="ticket-message" class="form-control" name="message" required></textarea>
    					</div>
    				</div>
    			</div>
    			<div class="modal-footer">
    				<a class="btn btn-default" data-dismiss="modal">Close</a>
    				<button type="submit" class="btn btn-primary">Send Support Ticket</button>
    			</div>
    		</div>
    	</div>
    	{!! BootForm::close() !!}	
    </div>
    <!-- /page content -->
@endsection

@push('scripts')
	<!-- Trumbowyg -->
   	<script src="{{ asset('js/trumbowyg.min.js') }}"></script>
   	<script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
	<script type="text/javascript">
		$('#modals').on('show.bs.modal', function (e) {
			$('#ticket-message').trumbowyg({
				svgPath: '{{ asset('icons/icons.svg') }}'
			});
		})
		
	</script>

@endpush