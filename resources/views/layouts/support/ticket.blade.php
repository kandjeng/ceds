@extends('blank')

@push('stylesheets')
    <!-- Trumbowyg -->
    <link href="{{ asset('css/trumbowyg.min.css') }}" rel="stylesheet">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <h3>Support Ticket</h3>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2> {{$ticket->title}} </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <div class="row">
                            @if (Auth::user()->is_admin)
                            <div class="col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        {!! BootForm::open(['id' =>'addResponse', 'url' => route('editTicket'), 'method' => 'post','class'=>'text-right']) !!}
                                            <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                                            <select id="selectStatus" name="status" class="form-control admin-change">
                                                <option value="open">Open</option>
                                                <option value="on progress">On Progress</option>
                                                <option value="closed">Closed</option>
                                            </select>
                                            <select id="selectPriority" name="priority" class="form-control admin-change">
                                                <option value="0">Minor Problem</option>
                                                <option value="1">Medium Problem</option>
                                                <option value="2">Urgent</option>
                                            </select>
                                            <button type="submit" class="btn btn-primary">Change ticket state</button>
                                        {!! BootForm::close() !!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-xs-12 col-md-10 col-md-offset-1">
                                <div class="panel panel-dark panel-default">
                                    <div class="panel-heading">
                                        <h4 class="pull-right status label {{$status[$ticket->status]}} ">{{$ticket->status}}</h4>
                                        Submitted by : <strong>{{$ticket->author->name}}</strong>
                                        
                                    </div>
                                    <div class="panel-body panel-message">
                                        {!! $ticket->message !!}
                                    </div>
                                    <div class="panel-footer">
                                        <span class="pull-right"><em>Posted {{$ticket->created_at->diffForHumans()}}</em></span>
                                        <h4 class="label {{$priority[$ticket->priority]['class']}}">{{$priority[$ticket->priority]['label']}}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @foreach($responses as $response)
                        <div class="row response">
                            <div class="col-xs-12 col-md-10 row {{$response->author->is_admin?'receiver':''}}">
                                <div class="avatar col-sm-2 col-xs-3">
                                    <img src="{{ Gravatar::src($response->author->email) }}">
                                </div>
                                <div class="panel panel-default col-xs-9 col-sm-10">
                                    <div class="panel-heading">
                                        <div class="pull-right res-date">Posted {{$response->created_at->diffForHumans()}}</div>
                                        <div class="res-title">{{$response->author->name}}</div>
                                    </div>
                                    <div class="panel-body">
                                        {!! $response->message !!}
                                    </div>
                                </div>
                            </div> 
                        </div>
                        @endforeach
                        
                        <div class="text-center">
                            <a href="{{ url('cpadmin/support') }}" class="btn btn-default">return to support center</a>
                            @if ($ticket->status!='closed')
                            <button class="btn btn-primary" data-toggle="modal" data-target="#modals">Reply to this ticket</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
        {!! BootForm::open(['id' =>'addResponse', 'url' => route('addResponse'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reply to this ticket</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-md-8 col-xs-12 col-md-offset-2">
                        <div class="form-group">
                            <label class="control-label" for="ticket-message">Message</label>
                            <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                            <textarea id="ticket-message" class="form-control" name="message" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">Close</a>
                    <button type="submit" class="btn btn-primary">Send Reply</button>
                </div>
            </div>
        </div>
        {!! BootForm::close() !!}   
    </div>
    <!-- /page content -->
@endsection

@push('scripts')
    <!-- Trumbowyg -->
    <script src="{{ asset('js/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('js/trumbowyg.cleanpaste.min.js') }}"></script>
    <script type="text/javascript">
        $('#modals').on('show.bs.modal', function (e) {
            $('#ticket-message').trumbowyg({
                svgPath: '{{ asset('icons/icons.svg') }}'
            });
        })
        @if (Auth::user()->is_admin)
        $(document).ready(function(){
            $('#selectStatus').find('option[value="{{$ticket->status}}"]').attr("selected",true);
            $('#selectPriority').find('option[value="{{$ticket->priority}}"]').attr("selected",true);
            changeColor1('{{$ticket->status}}');
            changeColor2('{{$ticket->priority}}');
        });
        function changeColor1(val){
            elem = $('#selectStatus');
            elem.css('color','white');
            switch(val){
                case '0' : elem.css('background','white');break;
                case 'open' : elem.css('background','#5cb85c');break;
                case 'on progress' : elem.css('background','#f0ad4e');break;
                case 'closed' : elem.css('background','#777'); break;
            }
        }
        function changeColor2(val){
            elem = $('#selectPriority');
            elem.css('color','white');
            switch(val){
                case '0' : elem.css('background','#5bc0de');break;
                case '1' : elem.css('background','#f0ad4e');break;
                case '2' : elem.css('background','#d9534f');break;
                
            }
        }
        $('#selectStatus').change(function(){
            changeColor1($(this).val());
        });
        $('#selectPriority').change(function(){
            changeColor2($(this).val());
        });
        @endif
    </script>
@endpush